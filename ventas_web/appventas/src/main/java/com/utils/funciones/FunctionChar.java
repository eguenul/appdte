/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.utils.funciones;

/**
 *
 * @author esteban
 */
public class FunctionChar {
    

public String formatoTexto(String parmcadena){
    
  String cadena = parmcadena;
  
  cadena = cadena.replaceAll("Ñ","N");
  cadena = cadena.replaceAll("ñ","n");
  
  /* LETRAS MINUSCULAS */
  cadena = cadena.replaceAll("á","a");
  cadena = cadena.replaceAll("é","e");
  cadena = cadena.replaceAll("í","i");
  cadena = cadena.replaceAll("ó","u");
  cadena = cadena.replaceAll("ú","u");
  
  /* LETRAS MAYUSCULAS */
  cadena = cadena.replaceAll("Á","A");
  cadena = cadena.replaceAll("É","E");
  cadena = cadena.replaceAll("Í","I");
  cadena = cadena.replaceAll("Ó","O");
  cadena = cadena.replaceAll("Ú","U"); 
  cadena = cadena.replaceAll("&","Y");
  

  return cadena;    
    
}




}
