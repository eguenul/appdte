/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdte.sii.utilidades;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Enumeration;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class CertificadoValidator {
    
 public boolean validaCertificado(String pathcertificado, String clave) throws ParserConfigurationException, SAXException, IOException, KeyStoreException, NoSuchAlgorithmException, UnrecoverableEntryException, CertificateException{
        ConfigAppDTE objConfigAppDTE = new ConfigAppDTE();
        KeyStore p12 = KeyStore.getInstance("pkcs12");
        p12.load(new FileInputStream(objConfigAppDTE.getPathcert()+ pathcertificado + ".pfx"), clave.trim().toCharArray());
        Enumeration e = p12.aliases();
        String alias = (String) e.nextElement();
        System.out.println("Alias certifikata:" + alias);
        KeyStore.PrivateKeyEntry keyEntry = (KeyStore.PrivateKeyEntry) p12.getEntry(alias, new KeyStore.PasswordProtection(clave.trim().toCharArray()));
        X509Certificate cert = (X509Certificate) keyEntry.getCertificate();
        return isCertificadoVencido(cert);
}
    
    
    
    
    
    
    
public  boolean isCertificadoVencido(X509Certificate certificado) {
        Date fechaActual = new Date(); // Obtiene la fecha actual
        return certificado.getNotAfter().before(fechaActual); // Compara la fecha de expiración con la fecha actual
}
    
    
    
}
