/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdte.sii.utilidades;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 * @author esteban
 */
public class byteXML {
 
  public void cleanXML(String filePath) throws IOException{
    System.setProperty("file.encoding", "ISO-8859-1");
    byte[] bytes = Files.readAllBytes(Paths.get(filePath));

     String strDATA = new String(bytes, "ISO-8859-1");
      String strCleaned = strDATA.replaceAll("&#13;", "");
  
      byte[] bytes2 = strCleaned.getBytes("ISO-8859-1");
      try (OutputStream os = new FileOutputStream(filePath)) {
          os.write(bytes2);
      }
 
  }
    
    
    
}
