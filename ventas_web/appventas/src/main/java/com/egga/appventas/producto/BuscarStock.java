/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appventas.producto;

import com.egga.appventas.movimientos.StockModel;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author esteban
 */


@WebServlet("/buscarStock")
public class BuscarStock extends HttpServlet {

    
@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
int productocod = Integer.parseInt(request.getParameter("ProductoCod"));
int bodegaid = Integer.parseInt(request.getParameter("BodegaId"));
int empresaid = (int) request.getSession().getAttribute("empresaid");
 
ProductoModel objProductoModel = new ProductoModel(empresaid);
    try {
        int productoid = objProductoModel.getProductoId(productocod);
        StockModel objStockModel = new StockModel();  
        BigDecimal SaldoDisponible = objStockModel.getSaldo(bodegaid,productoid);
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.print("{");
        out.print("\"saldodisponible\":" + "\""+  String.valueOf(SaldoDisponible)+"\""   +"}");


        
    } catch (SQLException ex) {
        Logger.getLogger(BuscarStock.class.getName()).log(Level.SEVERE, null, ex);
    }

}


}
