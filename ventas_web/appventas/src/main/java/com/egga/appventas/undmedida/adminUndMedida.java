/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appventas.undmedida;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class adminUndMedida extends HttpServlet {
    
@Override
public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    UndMedidaModel objUndMedidaModel = new UndMedidaModel();
    ArrayList<UndMedida> arraylistundmedida = objUndMedidaModel.listUndMedida();
         
    request.getSession().setAttribute("idUndMedida", "");
    request.getSession().setAttribute("UndMedidaNom", "");
    request.getSession().setAttribute("UndMedidaDes", "");
    request.getSession().setAttribute("arraylistundmedida",arraylistundmedida);
    request.getSession().setAttribute("ACC","GRABAR");
    
    getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/jsp/undmedidaview/addunidad.jsp").forward(request,response);
    
}



@Override
public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        
        
    try {
        int idUndMedida = Integer.parseInt(request.getParameter("idUndMedida"));
        UndMedidaModel objUndMedidaModel = new UndMedidaModel();
        UndMedida objUndMedida = objUndMedidaModel.getUndMedida(idUndMedida);
        request.getSession().setAttribute("idUndMedida", objUndMedida.getIdundmedida());
        request.getSession().setAttribute("UndMedidaDes", objUndMedida.getUndmedidades());
        request.getSession().setAttribute("UndMedidaNom", objUndMedida.getUndmedidanomen());
        
        ArrayList<UndMedida> arraylistundmedida = objUndMedidaModel.listUndMedida();
        request.getSession().setAttribute("arraylistundmedida",arraylistundmedida);
        
        request.getSession().setAttribute("ACC","UPDATE");
        getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/jsp/undmedidaview/addunidad.jsp").forward(request,response);
    } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
        Logger.getLogger(adminUndMedida.class.getName()).log(Level.SEVERE, null, ex);
    }
                    
                        
                        
                        
                        
        }
        
        
    
    }
    









