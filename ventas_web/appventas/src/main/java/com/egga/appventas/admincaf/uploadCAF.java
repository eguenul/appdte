package com.egga.appventas.admincaf;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.egga.appdte.sii.utilidades.ConfigAppDTE;

@MultipartConfig
public class uploadCAF extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(uploadCAF.class.getName());

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // Recupera el nombre de usuario desde la sesión
            String login = (String) request.getSession().getAttribute("login");
            
            // Recupera la configuración de la aplicación
            ConfigAppDTE objconfig = new ConfigAppDTE();
            String pathCAF = objconfig.getPathcaf();
           String fileName = "";
            // Obtiene las partes del archivo cargado
            for (Part part : request.getParts()) {
                 fileName = getFileName(part);
                if (fileName != null && !fileName.isEmpty()) {
                    File file = new File(pathCAF + login + fileName);
                    try (InputStream input = part.getInputStream()) {
                        // Guarda el archivo en el directorio especificado
                        part.write(file.getAbsolutePath());
                        LOGGER.info("File uploaded successfully: " + fileName);
                    }
                }
            }

            // Procesa el archivo cargado
            int empresaid = (int) request.getSession().getAttribute("empresaid");
            saveCAf objsaveCAF = new saveCAf();
            if (!objsaveCAF.readCAF(empresaid, pathCAF + login + fileName)) {
                response.sendRedirect("messageview/errorfile.html");
            } else {
                objsaveCAF.guardarCAF(empresaid, pathCAF + login + fileName);
                response.sendRedirect("messageview/cafok.html");
            }

        } catch (IllegalStateException | ParserConfigurationException | SAXException | SQLException | ClassNotFoundException ex) {
            LOGGER.log(Level.SEVERE, "Error processing file upload", ex);
            response.sendRedirect("messageview/errorfile.html");
        }
    }

    // Helper method to extract the file name from the Part header
    private String getFileName(Part part) {
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }
}


      
