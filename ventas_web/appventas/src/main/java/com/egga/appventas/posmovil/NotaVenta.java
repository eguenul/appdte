/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appventas.posmovil;

import com.egga.appventas.documento.DocumentoModel;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author esteban
 */
public class NotaVenta extends HttpServlet {
     @Override
  public void  doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{ 
  
         try {
             DocumentoModel objDocumentoModel = new DocumentoModel();
             request.getSession().setAttribute("nombredoc", objDocumentoModel.getNombreDocCodSii("802"));
             request.getSession().setAttribute("TipoDocumento", objDocumentoModel.getIddocumento2(802));
             request.getSession().setAttribute("coddocsii", "802");
             response.sendRedirect("posmovil");
         } catch (SQLException ex) {
             Logger.getLogger(NotaVenta.class.getName()).log(Level.SEVERE, null, ex);
         }

  }
  }
