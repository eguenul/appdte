
package com.egga.appventas.posmovil;


import com.egga.appventas.documento.DocumentoModel;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Boleta extends HttpServlet {
      
   @Override
   public void  doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        
       try {
           DocumentoModel objDocumentoModel = new DocumentoModel();
           
           request.getSession().setAttribute("nombredoc", objDocumentoModel.getNombreDocCodSii("39"));
           request.getSession().setAttribute("TipoDocumento", objDocumentoModel.getIddocumento2(39));
           request.getSession().setAttribute("coddocsii", "39");
           response.sendRedirect("posmovil");
       } catch (SQLException ex) {
           Logger.getLogger(Boleta.class.getName()).log(Level.SEVERE, null, ex);
       }
   }
    
}
