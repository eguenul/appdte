/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appventas.movimientos;

import com.egga.appventas.bodega.Bodega;
import com.egga.appventas.bodega.BodegaModel;
import com.egga.appventas.referencia.Referencia;
import com.egga.appventas.referencia.ReferenciaModel;
import com.egga.appventas.tpoventa.TpoVenta;
import com.egga.appventas.tpoventa.TpoVentaModel;
import com.egga.appventas.cliprov.CliProv;
import com.egga.appventas.cliprov.CliProvModel;
import com.egga.appventas.documento.Documento;
import com.egga.appventas.documento.DocumentoModel;
import com.egga.appventas.fpago.FPago;
import com.egga.appventas.fpago.FPagoModel;
import com.egga.appventas.producto.Producto;
import com.egga.appventas.producto.ProductoModel;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */

public class dtecompra extends HttpServlet{
     
    
@Override    
public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            if(request.getSession().getAttribute("loginauth")==null){
         request.getRequestDispatcher("login").forward(request, response); 
       }
     
        
        
    int empresaid =  (int) request.getSession().getAttribute("empresaid");
        request.getSession().setAttribute("empresaid", empresaid);
    try {
           
        
        
        
    request.getSession().setAttribute("referencia","no");
        
    request.getSession().setAttribute("ReferenciaFlag",0);
    request.getSession().setAttribute("MovimientoId",0);
       
    BodegaModel objBodegaModel = new BodegaModel(empresaid);
    ArrayList<Bodega> arraylistbodega = objBodegaModel.listallBodegas();
    request.getSession().setAttribute("arraylistbodega", arraylistbodega);
       
        
        
        
    DespachoModel objDespacho = new DespachoModel();    
    List<Despacho> arraydespacho = objDespacho.listDespacho();
          
       
    TrasladoModel objTraslado = new TrasladoModel();    
    List<Traslado> arraytraslado = objTraslado.listTraslado();
    
       
       
    DocumentoModel objDocumentoModel = new DocumentoModel();
    List<Documento> arraylistdocumento = objDocumentoModel.listDocuments();
      
    request.getSession().setAttribute("coddocsii","46");
    request.getSession().setAttribute("nombredoc", objDocumentoModel.getNombreDocCodSii("46"));
    request.getSession().setAttribute("TipoDocumento", objDocumentoModel.getIddocumento2(46));
  
      
      
      
      
      
      CliProvModel objCliProvModel = new CliProvModel(empresaid);
      ArrayList<CliProv> arraylistcliprov = objCliProvModel.listaCliProv(0);
      
      ProductoModel objProductoModel = new ProductoModel(empresaid);
      ArrayList<Producto>  arrayproducto = objProductoModel.listProducto(0);
      
      FPagoModel objFPagoModel =  new FPagoModel();
      
      ArrayList<FPago> arrayfpago = objFPagoModel.listFpago();
              
      TpoVentaModel objTpoVentaModel = new TpoVentaModel();
      ArrayList<TpoVenta> arraytpoventa = objTpoVentaModel.listTpoVenta();
      
      ReferenciaModel objReferenciaModel = new ReferenciaModel();
      ArrayList<Referencia> arrayreferencia = objReferenciaModel.listTpoReferencia();
      request.getSession().setAttribute("arrayreferencia",arrayreferencia);
      
       
     CliProv objcliprov = new CliProv();
     request.getSession().setAttribute("CliProvCod","");
     request.getSession().setAttribute("CliProvRaz","");
     request.getSession().setAttribute("CliProvRut","");
     request.getSession().setAttribute("CliProvDir","");
     request.getSession().setAttribute("CliProvCom","");
     request.getSession().setAttribute("CliProvGir","");
      
      
    request.getSession().setAttribute("servletName",arraylistdocumento);
    request.getSession().setAttribute("arraylistcliprov",arraylistcliprov);
    request.getSession().setAttribute("objcliprov",objcliprov);
    request.getSession().setAttribute("arrayproducto",arrayproducto);   
    
    request.getSession().setAttribute("arraydespacho",arraydespacho);
    request.getSession().setAttribute("arraytraslado",arraytraslado);
         
    
    request.getSession().setAttribute("arrayfpago",arrayfpago);
    
    request.getSession().setAttribute("arraytpoventa",arraytpoventa);
    
    
    request.getSession().setAttribute("modulo", "dtecompra");
    getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/jsp/movimientoview/posmovilcompra.jsp").forward(request,response);
  
       
    }catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
        Logger.getLogger(MovimientoServlet.class.getName()).log(Level.SEVERE, null, ex);
    
    
    }
         
}


@Override
public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
 int empresaid = (int) request.getSession().getAttribute("empresaid");
          String acc = request.getParameter("ACC");
         try {
              BodegaModel objBodegaModel = new BodegaModel(empresaid);
           ArrayList<Bodega> arraylistbodega = objBodegaModel.listallBodegas();
           request.getSession().setAttribute("arraylistbodega", arraylistbodega);
       ReferenciaModel objReferenciaModel = new ReferenciaModel();
                ArrayList<Referencia> arrayreferencia = objReferenciaModel.listTpoReferencia();
                request.getSession().setAttribute("arrayreferencia",arrayreferencia);
      
 
        if("BUSCAR".equals(acc)){    
            
                
                request.getSession().setAttribute("ReferenciaFlag",0);
                request.getSession().setAttribute("MovimientoId",0);
                 
               DespachoModel objDespacho = new DespachoModel();    
               request.getSession().setAttribute("arraydespacho",objDespacho.listDespacho());
       
                DocumentoModel objDocumentoModel;
                objDocumentoModel = new DocumentoModel();
                request.getSession().setAttribute("coddocsii","46");
  
            
                request.getSession().setAttribute("nombredoc", objDocumentoModel.getNombreDocCodSii("46"));
  
      
                List<Documento> arraylistdocumento = objDocumentoModel.listDocuments();
                CliProvModel objCliProvModel = new CliProvModel(empresaid);
                
                CliProv objCliProv = objCliProvModel.searchCliProv(Integer.parseInt(request.getParameter("CliProvCod")));   
                ArrayList<CliProv> arraylistcliprov = objCliProvModel.listaCliProv(0);
                
                ProductoModel objProductoModel = new ProductoModel(empresaid);
                ArrayList<Producto>  arrayproducto = objProductoModel.listProducto(0);
                request.getSession().setAttribute("arrayproducto",arrayproducto);
                
                
                request.getSession().setAttribute("servletName",arraylistdocumento);
                request.getSession().setAttribute("arraylistcliprov",arraylistcliprov);
                request.getSession().setAttribute("objcliprov",objCliProv);
                request.getSession().setAttribute("referencia","no");
                
                request.getSession().setAttribute("modulo", "dtecompra");
                
                
       
                
                
                
                getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/jsp/movimientoview/posmovilcompra.jsp").forward(request,response);
            } 
                
                    
                } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException | IOException | NumberFormatException ex) {
                    Logger.getLogger(MovimientoServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            
                     
         
}
     
     
     
}
