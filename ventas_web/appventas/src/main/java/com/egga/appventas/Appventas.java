package com.egga.appventas;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

public class Appventas {

    public static void main(String[] args) throws Exception {
        // Crear una instancia del servidor en el puerto 8080
        Server server = new Server(8080);
        // Configurar el contexto web para JSP y servlets
        WebAppContext context = new WebAppContext();
          
        context.setContextPath("/AppVentas");
        context.setResourceBase("src/main/webapp"); // Carpeta con los archivos JSP
        context.setInitParameter("org.eclipse.jetty.servlet.Default.dirAllowed", "false"); 
        context.setParentLoaderPriority(true); // Configurar para que Jetty use su propio ClassLoader para servlets
       // Agregar el contexto web al servidor
        server.setHandler(context);
       // Iniciar el servidor
        server.start();
        server.join();
    }
}
