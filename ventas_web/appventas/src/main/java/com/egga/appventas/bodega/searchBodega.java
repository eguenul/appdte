/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appventas.bodega;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class searchBodega  extends HttpServlet {
    
    @Override
public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        try {
            int empresaid =  (int) request.getSession().getAttribute("empresaid");
            request.getSession().setAttribute("empresaid", empresaid);
            
            String acc = request.getParameter("ACC");
            BodegaModel objBodegaModel = new BodegaModel(empresaid);
            
            switch(acc){
                
                case "searchCod":
                 
                    
                int bodegacod = Integer.parseInt(request.getParameter("BodegaCod"));
                ArrayList<Bodega> arraylistbodega = objBodegaModel.listBodegaCod(bodegacod);
                request.getSession().setAttribute("arraylistbodega", arraylistbodega);
                getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/jsp/bodegaview/listbodega2.jsp").forward(request,response);    
                  
                    
                    
                break;
                    
                case "searchNom":
                System.out.print("error");
                String bodeganom = request.getParameter("BodegaNom");
                ArrayList<Bodega> arraylistbodega2 = objBodegaModel.listBodegaNom(bodeganom);
                request.getSession().setAttribute("arraylistbodega", arraylistbodega2);
                getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/jsp/bodegaview/listbodega2.jsp").forward(request,response);    
                break;
                    
                    
            }   
        
           } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
            Logger.getLogger(searchBodega.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    
}


@Override
public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{

    
    
}



}
