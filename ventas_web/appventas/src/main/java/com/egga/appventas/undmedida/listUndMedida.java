package com.egga.appventas.undmedida;

import java.io.IOException;
import java.util.ArrayList;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;






public class listUndMedida extends HttpServlet{
@Override
public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
    UndMedidaModel objUndMedidaModel = new UndMedidaModel();
    ArrayList<UndMedida> arraylistundmedida = objUndMedidaModel.listUndMedida();
    request.getSession().setAttribute("arraylistundmedida",arraylistundmedida);
    getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/jsp/undmedidaview/listundmedida2.jsp").forward(request,response);
}
}
