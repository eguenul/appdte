package com.egga.appventas.web;

import com.egga.appventas.empresa.Empresa;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

public class IndexServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {        
        // Verificar si el usuario está autenticado
        if (request.getSession().getAttribute("loginauth") == null) {
            response.sendRedirect("login"); // Si no está autenticado, redirigir a login
            return;
        }

        // Verificar si la empresa está seleccionada
        if (request.getSession().getAttribute("empresaid") == null) {
            response.sendRedirect("selempresa"); // Si no hay empresa seleccionada, redirigir
            return;
        }
        // Obtener la empresa de la sesión
        Empresa objEmpresa = (Empresa) request.getSession().getAttribute("Empresa");

        // Verificar que la empresa esté correctamente cargada
        if (objEmpresa == null || objEmpresa.getEmpresaid() == 0) {
            response.sendRedirect("selempresa"); // Redirigir a selección si es nula o incorrecta
            return;
        }
        // Todo está correcto, redirigir al JSP de índice
        request.getRequestDispatcher("/WEB-INF/jsp/index.jsp").forward(request, response);
    }

 
}



