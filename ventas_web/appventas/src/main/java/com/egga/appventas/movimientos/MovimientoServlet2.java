
package com.egga.appventas.movimientos;
import com.egga.appventas.tpoventa.TpoVenta;
import com.egga.appventas.tpoventa.TpoVentaModel;
import com.egga.appventas.bodega.Bodega;
import com.egga.appventas.bodega.BodegaModel;
import com.egga.appventas.documento.Documento;
import com.egga.appventas.documento.DocumentoModel;
import com.egga.appventas.fpago.FPago;
import com.egga.appventas.fpago.FPagoModel;
import com.egga.appventas.referencia.Referencia;
import com.egga.appventas.referencia.ReferenciaModel;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class MovimientoServlet2 extends HttpServlet {
    
@Override
public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
    if(request.getSession().getAttribute("loginauth")==null){
        request.getRequestDispatcher("login").forward(request, response);                                        
    }
    request.getSession().setAttribute("coddocsii", null);  
    
        
    }

 @Override
public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
     try {
        String acc = request.getParameter("ACC");
        
        
        
        
        int empresaid = (int) request.getSession().getAttribute("empresaid");
        
        
        
     
        
        
  
            request.getSession().setAttribute("coddocsii", null);
            
            BodegaModel objBodegaModel = new BodegaModel(empresaid);
            ArrayList<Bodega> arraylistbodega = objBodegaModel.listallBodegas();
            request.getSession().setAttribute("arraylistbodega", arraylistbodega);
            
            
            
            
            
            request.getSession().setAttribute("referencia","yes");
            int idmovimiento = Integer.parseInt(request.getParameter("Id"));
            
            
            MovimientoModel2 objMovimientoModel = new MovimientoModel2();
            Movimiento objMovimiento = objMovimientoModel.getData(idmovimiento);
            
            
            
            
            
            
            
            request.getSession().setAttribute("ReferenciaFlag",1);
            request.getSession().setAttribute("MovimientoId",idmovimiento);
            
            /* DATOSA CLIENTE PROVEEDOR */
            MovimientoModel objMovimientoModel2 = new MovimientoModel();
            Object[] arraymovimiento = objMovimientoModel2.showDocument(idmovimiento);
            
            
            
            request.getSession().setAttribute("CliProvCod",arraymovimiento[0]);
            request.getSession().setAttribute("CliProvRut",arraymovimiento[1]);
            request.getSession().setAttribute("CliProvRaz",arraymovimiento[2]);
            request.getSession().setAttribute("CliProvDir",arraymovimiento[4]);
            request.getSession().setAttribute("CliProvCom",arraymovimiento[5]);
            request.getSession().setAttribute("CliProvGir",arraymovimiento[3]);
            
            
            request.getSession().setAttribute("TotalNeto",arraymovimiento[11]);
            request.getSession().setAttribute("Exento",arraymovimiento[12]);
            request.getSession().setAttribute("Iva",arraymovimiento[13]);
            request.getSession().setAttribute("Total",arraymovimiento[14]);
            
            
            ArrayList<Object[]> arraydetalle = objMovimientoModel2.showDetails(idmovimiento);     
            request.getSession().setAttribute("arraydetalle", arraydetalle);
            
            
            
            Object[] arrayreferencia = objMovimientoModel.getReferencia(idmovimiento);
            
            int numdoc = (int) arrayreferencia[1];
            String fechadoc = (String) arrayreferencia[0];
            String docdes = (String) arrayreferencia[2];
            int codsii = (int) arrayreferencia[3];
            int tipodocref = (int) arrayreferencia[4];
            
            request.getSession().setAttribute("numdoc", numdoc);
            request.getSession().setAttribute("docdes", docdes);
            request.getSession().setAttribute("codsii", codsii);
            request.getSession().setAttribute("fechadoc", fechadoc);
            request.getSession().setAttribute("tipodocref", tipodocref);
            
            
            
            DespachoModel objDespacho = new DespachoModel();
            List<Despacho> arraydespacho = objDespacho.listDespacho();
            TrasladoModel objTraslado = new TrasladoModel();
            List<Traslado> arraytraslado = objTraslado.listTraslado();
            
            
            
            request.getSession().setAttribute("arraydespacho",arraydespacho);
            request.getSession().setAttribute("arraytraslado",arraytraslado);
            
            
            DocumentoModel objDocumentoModel2;
            objDocumentoModel2 = new DocumentoModel();
            List<Documento> arraylistdocumento2 = objDocumentoModel2.listDocuments();
            
            
            request.getSession().setAttribute("servletName",arraylistdocumento2);
            
            
            FPagoModel objFPagoModel =  new FPagoModel();
            
            ArrayList<FPago> arrayfpago = objFPagoModel.listFpago();
            
            TpoVentaModel objTpoVentaModel = new TpoVentaModel();
            ArrayList<TpoVenta> arraytpoventa = objTpoVentaModel.listTpoVenta();
            request.getSession().setAttribute("arrayfpago",arrayfpago);
            
            request.getSession().setAttribute("arraytpoventa",arraytpoventa);
            
           
            String CodSiiDoc = request.getParameter("CodSii");
            DocumentoModel objDocumento = new DocumentoModel();
            int tipodocumento = objDocumento.getIddocumento2(Integer.parseInt(CodSiiDoc));
            
            
            String nombredoc =  objDocumento.getNombreDoc(tipodocumento);
            request.getSession().setAttribute("nombredoc",nombredoc);
            request.getSession().setAttribute("TipoDocumento",tipodocumento);
            
            ReferenciaModel objReferenciaModel = new ReferenciaModel();
            ArrayList<Referencia> arrayreferencia2 = objReferenciaModel.listTpoReferencia();
            request.getSession().setAttribute("arrayreferencia",arrayreferencia2);
            getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/jsp/movimientoview/addmovimiento2.jsp").forward(request,response);
            
        
    } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
        Logger.getLogger(MovimientoServlet2.class.getName()).log(Level.SEVERE, null, ex);
    }
         
         

        
        

  
                

}
   

}
