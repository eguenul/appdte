package com.egga.appventas.usuarios;

import com.egga.appventas.include.Funciones;
import com.egga.appdte.sii.utilidades.ConfigAppDTE;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 * Servlet para la carga de certificados.
 */
@MultipartConfig
public class uploadCert extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(uploadCert.class.getName());

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // Recupera el nombre de usuario desde la sesión
            String login = (String) request.getSession().getAttribute("login");

            // Recupera la configuración de la aplicación
            ConfigAppDTE objconfig = new ConfigAppDTE();
            String pathCert = objconfig.getPathcert();
            String fileName = "";

            // Obtiene las partes del archivo cargado
            for (Part part : request.getParts()) {
                fileName = getFileName(part);
                if (fileName != null && !fileName.isEmpty()) {
                    // Genera un nombre único si el archivo ya existe
                    File file = new File(pathCert + login + fileName);
                    String uniqueFileName = generateUniqueFileName(file);

                    // Guarda el archivo en el directorio especificado
                    try (InputStream input = part.getInputStream()) {
                        part.write(uniqueFileName);
                        LOGGER.info("File uploaded successfully: " + uniqueFileName);
                    }

                    // Procesa el archivo cargado
                    Funciones objFunciones = new Funciones();
                    objFunciones.addCertificado(login, uniqueFileName);
                }
            }

            // Redirige a la página de éxito
            response.sendRedirect("messageview/cafok.html");

        } catch (IllegalStateException | ParserConfigurationException | SAXException | SQLException | ClassNotFoundException ex) {
            LOGGER.log(Level.SEVERE, "Error processing file upload", ex);
            response.sendRedirect("messageview/errorfile.html");
        }
    }

    // Método para extraer el nombre del archivo del encabezado Part
    private String getFileName(Part part) {
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    // Método para generar un nombre de archivo único si ya existe
    private String generateUniqueFileName(File file) {
        int count = 1;
        String baseName = file.getName();
        String nameWithoutExt = baseName.contains(".") ? baseName.substring(0, baseName.lastIndexOf('.')) : baseName;
        String extension = baseName.contains(".") ? baseName.substring(baseName.lastIndexOf('.')) : "";
        
        while (file.exists()) {
            file = new File(file.getParent(), nameWithoutExt + "(" + count + ")" + extension);
            count++;
        }
        return file.getAbsolutePath();
    }
}
