/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appventas.report;

import com.egga.appventas.bodega.Bodega;
import com.egga.appventas.bodega.BodegaModel;
import com.egga.appventas.include.ConfigAppVenta;
import com.egga.appdte.sii.utilidades.ConfigAppDTE;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import net.sf.jasperreports.engine.JRException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class InformeStock extends HttpServlet{
 @Override
  public void  doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
     try {
         int empresaid =  (int) request.getSession().getAttribute("empresaid");
         request.getSession().setAttribute("empresaid", empresaid);
         
         BodegaModel objBodegaModel = new BodegaModel(empresaid);
         ArrayList<Bodega> arraylistbodega = objBodegaModel.listallBodegas();
         request.getSession().setAttribute("arraylistbodega", arraylistbodega);
         
         getServletConfig().getServletContext().getRequestDispatcher("/reportview/stockbodega.jsp").forward(request,response);
     } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
         Logger.getLogger(InformeStock.class.getName()).log(Level.SEVERE, null, ex);
     }
     }
  
  
  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
     try {
         int empresaid =  (int) request.getSession().getAttribute("empresaid");
         request.getSession().setAttribute("empresaid", empresaid);
         
         
         String extension =request.getParameter("extension");
         String bodegaid =request.getParameter("Bodega");
         
         ConfigAppVenta  objconfig = new ConfigAppVenta();
         ConfigAppDTE objconfigdte = new ConfigAppDTE();
         Report objReport = new Report("stockbodega",objconfig.getPathdownload(),"stock"+String.valueOf(empresaid) +"."+extension, objconfigdte.getPathpdf());
         
         request.getSession().setAttribute("nombredocumento", "stock"+String.valueOf(empresaid));
         
         objReport.setParameters("aux_bodegaid", bodegaid);
         objReport.setParameters("aux_empresaid", String.valueOf(empresaid));
         request.getSession().setAttribute("extension",extension);
         if( "xls".equals(extension)){
             objReport.showExcel();
         }else{
             objReport.showReport();
             
         }
         getServletConfig().getServletContext().getRequestDispatcher("/reportview/download.jsp").forward(request,response);
     } catch (ParserConfigurationException | SAXException | SQLException | ClassNotFoundException | JRException ex) {
         Logger.getLogger(InformeStock.class.getName()).log(Level.SEVERE, null, ex);
     }
         
      
      
  }
  
  
  
  
}
