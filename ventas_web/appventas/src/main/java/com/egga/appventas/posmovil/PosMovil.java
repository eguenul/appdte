
package com.egga.appventas.posmovil;

import com.egga.appventas.cliprov.CliProv;
import com.egga.appventas.cliprov.CliProvModel;
import com.egga.appventas.movimientos.MovimientoService;
import com.egga.appventas.movimientos.MovimientoServlet;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class PosMovil extends HttpServlet {
          
    @Override
   public void  doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        
        try {
            if(request.getSession().getAttribute("loginauth")==null){
                request.getRequestDispatcher("login").forward(request, response);
            }
            int empresaid =  (int) request.getSession().getAttribute("empresaid");
            request.getSession().setAttribute("empresaid", empresaid);
            MovimientoService objMovimientoService  = new MovimientoService(empresaid);
            configurarSesion(request, objMovimientoService);
            request.getSession().setAttribute("CliProvCod","");
            request.getSession().setAttribute("CliProvRaz","");
            request.getSession().setAttribute("CliProvRut","");
            request.getSession().setAttribute("CliProvDir","");
            request.getSession().setAttribute("CliProvCom","");
            request.getSession().setAttribute("CliProvGir","");
            getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/jsp/movimientoview/posmovil.jsp").forward(request,response);
        } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
            enviarError(response,"ERROR DE CONEXION. Por favor, inténtelo más tarde.");
            Logger.getLogger(PosMovil.class.getName()).log(Level.SEVERE, null, ex);
        }
   
   }
    
   
    @Override
   public void  doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
               int empresaid = (int) request.getSession().getAttribute("empresaid");
          String acc = request.getParameter("ACC");
         try {
        
 
        if("BUSCAR".equals(acc)){    
                CliProvModel objCliProvModel = new CliProvModel(empresaid);
                CliProv objCliProv = objCliProvModel.searchCliProv(Integer.parseInt(request.getParameter("CliProvCod")));
                MovimientoService objMovimientoService  = new MovimientoService(empresaid);
                configurarSesion(request, objMovimientoService);
                request.getSession().setAttribute("CliProvCod",objCliProv.getCliprovcod());
                request.getSession().setAttribute("CliProvRaz",objCliProv.getCliprovraz());
                request.getSession().setAttribute("CliProvRut",objCliProv.getCliprovrut());
                request.getSession().setAttribute("CliProvDir",objCliProv.getCliprovdir());
                request.getSession().setAttribute("CliProvCom",objCliProv.getCliprovcom());
                request.getSession().setAttribute("CliProvGir",objCliProv.getCliprovgir());                   
                getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/jsp/movimientoview/posmovil.jsp").forward(request,response);
            } 
                
                    
                } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException | IOException | NumberFormatException ex) {
                    enviarError(response,"ERROR DE CONEXION. Por favor, inténtelo más tarde.");
                    Logger.getLogger(MovimientoServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
   
   
   }
   
   


private void configurarSesion(HttpServletRequest request, MovimientoService objMovimientoService) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
            request.getSession().setAttribute("servletName",objMovimientoService.listaDocumento());
            request.getSession().setAttribute("arraylistcliprov",objMovimientoService.listaCliProv());
            request.getSession().setAttribute("arrayproducto",objMovimientoService.listProducto());
            request.getSession().setAttribute("arraydespacho",objMovimientoService.listaDespacho());
            request.getSession().setAttribute("arraytraslado",objMovimientoService.listaTraslado());
            request.getSession().setAttribute("arrayfpago",objMovimientoService.listFormaPago());
            request.getSession().setAttribute("arraytpoventa",objMovimientoService.listTpoVenta());
            request.getSession().setAttribute("arraylistbodega",objMovimientoService.listBodegas());
            request.getSession().setAttribute("arrayreferencia",objMovimientoService.listTpoReferencia());
            request.getSession().setAttribute("modulo", "movimiento");
            request.getSession().setAttribute("ReferenciaFlag",0);
            request.getSession().setAttribute("MovimientoId",0);
               

}

private void enviarError(HttpServletResponse response, String mensaje) throws IOException {
    try (var writer = response.getWriter()) {
        writer.println("<html>");
        writer.println("<head><title>ERROR</title>");
        writer.println("<style>body { font-family: Arial, sans-serif; background-color: #f8d7da; color: #721c24; }" +
                       "h1 { color: #c7254e; } " +
                       "p { font-size: 1.2em; }</style>");
        writer.println("</head>");
        writer.println("<body>");
        writer.println("<h1>ERROR</h1>");
        writer.println("<p>" + mensaje + "</p>");
        writer.println("<p><a href=\"javascript:history.back();\">Volver a la página anterior</a></p>");
        writer.println("</body>");
        writer.println("</html>");
    }
}

}