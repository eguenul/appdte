/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appventas.movimientos;

import com.egga.appventas.empresa.Empresa;
import com.egga.appventas.empresa.EmpresaModel;
import com.egga.appventas.include.PrintNota;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author esteban
 */
public class PrintNotaServlet extends HttpServlet {
 @Override
 public void  doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{ 
     getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/jsp/movimientoview/shownota.jsp").forward(request,response);
                 
}






@Override
 public void  doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{ 
     
     try {
         int empresaid = (int)request.getSession().getAttribute("empresaid");
         int idmovimiento = Integer.parseInt(request.getParameter("Id"));
         PrintNota objPrint = new PrintNota();
         objPrint.printPDF(idmovimiento, empresaid);
         request.getSession().setAttribute("nombredocumento","NOTAPEDIDO");
         getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/jsp/movimientoview/shownota.jsp").forward(request,response);
     } catch (Exception ex) {
         Logger.getLogger(PrintNotaServlet.class.getName()).log(Level.SEVERE, null, ex);
     }
                 
}




}