/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appventas.movimientos;

import com.egga.appventas.empresa.Empresa;
import com.egga.appventas.empresa.EmpresaModel;
import com.egga.appdte.sii.utilidades.PrintBOLETA;
import com.egga.appdte.sii.utilidades.PrintDTE;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletResponse;
public class PrintDTEServlet extends HttpServlet {

    
    

@Override
public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
       
       getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/jsp/movimientoview/successfull.jsp").forward(request,response);
  
    
}    


@Override
public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
    try {
        
        int empresaid = (int)request.getSession().getAttribute("empresaid");
        EmpresaModel objEmpresaModel = new EmpresaModel();
        Empresa objEmpresa = new Empresa();
        objEmpresa = objEmpresaModel.getData(empresaid);
        String rutempresa = objEmpresa.getEmpresarut();
        int idmovimiento = Integer.parseInt(request.getParameter("Id"));
        BlobDTE objblobdte = new BlobDTE();
        objblobdte.getXMLDTE(idmovimiento);
        MovimientoModel datamodel = new MovimientoModel();
        Object[] documentdata = datamodel.showDocument(idmovimiento);
      
        int numcorrelativo = (int) documentdata[9];
        String codsii =(String) documentdata[8];
       
        
        
        PrintDTE objPrint = new PrintDTE();
        PrintBOLETA objPrintBOLETA = new PrintBOLETA();
        
        
        String arrayrutempresa[] = rutempresa.split("-");
        String auxrutempresa = arrayrutempresa[0];
        
        if("39".equals(codsii) || "41".equals(codsii)){
             objPrintBOLETA.printBOLETA(arrayrutempresa[0]+"F"+String.valueOf(numcorrelativo)+"T"+ (String) codsii);
     
        }else{
        objPrint.printDTE(arrayrutempresa[0]+"F"+String.valueOf(numcorrelativo)+"T"+ (String) codsii);
        }
        
        request.getSession().setAttribute("nombredocumento","ENVDTE"+auxrutempresa.trim()+"F"+String.valueOf(numcorrelativo)+"T"+codsii);
        request.getSession().setAttribute("tipovista","impresion");
        getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/jsp/movimientoview/successfull.jsp").forward(request,response);
  
    } catch (Exception ex) {
        Logger.getLogger(PrintDTEServlet.class.getName()).log(Level.SEVERE, null, ex);
    }
                            
                               
}

    
}
