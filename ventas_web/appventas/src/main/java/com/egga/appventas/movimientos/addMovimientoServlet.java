package com.egga.appventas.movimientos;

import com.egga.appdte.sii.utilidades.CertificadoValidator;
import com.egga.appventas.cliprov.CliProv;
import com.egga.appventas.cliprov.CliProvModel;
import com.egga.appventas.documento.DocumentoModel;
import com.egga.appventas.empresa.Empresa;
import com.egga.appventas.empresa.EmpresaModel;
import com.egga.appventas.include.Funciones;
import com.egga.appventas.include.PrintNota;
import com.egga.appventas.producto.Producto;
import com.egga.appventas.producto.ProductoModel;
import com.egga.appventas.usuarios.Usuario;
import com.egga.appventas.usuarios.UsuarioModel;
import com.egga.appdte.sii.utilidades.ConfigAppDTE;
import com.egga.appdte.sii.utilidades.FuncionesCAF;
import com.egga.appdte.sii.utilidades.PrintBOLETA;
import com.egga.appdte.sii.utilidades.PrintDTE;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
/**
 * Servlet para manejar la adición de movimientos de productos en la aplicación.
 * Este servlet procesa las solicitudes POST para agregar nuevos movimientos,
 * validando los certificados digitales, comprobando el stock de productos
 * y recopilando información de los formularios para insertar registros en la base de datos.
 * 
 * @author Esteban
 */





public class addMovimientoServlet extends HttpServlet { 
    @Override
     public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         try {
                int empresaid = (int) request.getSession().getAttribute("empresaid");
    
               
       if(request.getSession(true).getAttribute("loginauth")==null){
         request.getRequestDispatcher("login").forward(request, response); 
       }
       
       /* CONSULTA CERTIFICADO DIGITAL */
           ConfigAppDTE objConfigAppDTE = new ConfigAppDTE();
           String login = (String) request.getSession().getAttribute("login");
           String sFichero = objConfigAppDTE.getPathcert()+login+".pfx";
           File fichero = new File(sFichero);
        
           
         UsuarioModel objUsuarioModel = new UsuarioModel();
         Usuario objUsuario = objUsuarioModel.getUsuario(login); 
        
         
         
           /* valido que el certificado exista */
           /* En caso de no existir me redirigira a una pagina de error
           
           */
          
           if(!fichero.exists()){
                response.sendRedirect("messageview/errorcertificado.html");
               return; 
              }
           
         
         
         
         
        
/* 
 * Se crea una instancia del validador de certificados digitales.
 * A continuación, se valida el certificado digital asociado al usuario.
 * Si el certificado no es válido, se redirige al usuario a una página de error.
 */
         CertificadoValidator objValidador = new CertificadoValidator();
       
/* 
 * Verifico si el certificado digital es válido.
 * En caso de que no sea válido, se redirige a la página de error correspondiente.
 */

  try{
          if(objValidador.validaCertificado(login, objUsuario.getClavefirma())==true){
              response.sendRedirect("messageview/errorcertificado.html");           
              return;
            } 
  }          
              catch(NullPointerException e){
                  try (var writer = response.getWriter()) { // Uso de try-with-resources
                writer.println("<html>");
                writer.println("<head><title>ERROR</title>");
                writer.println("<style>body { font-family: Arial, sans-serif; background-color: #f8d7da; color: #721c24; }" +
                               "h1 { color: #c7254e; } " +
                               "p { font-size: 1.2em; }</style>"); // Estilos CSS básicos
                writer.println("</head>");
                writer.println("<body>");
                writer.println("<h1>ERROR</h1>");
                writer.println("<p>ERROR DE CLAVE FIRMA DIGITAL.</p>");
                writer.println("<p><a href=\"javascript:history.back();\">Volver a la página anterior</a></p>"); // Enlace para volver atrás usando JavaScript
                writer.println("</body>");
                writer.println("</html>");
           
                  return;
              }
             } 
         
           
           
         
           /* COMPRUEBO LA VALIDACION DE STOCK */
                  
           int idBodega = Integer.parseInt(request.getParameter("Bodega"));
           int flag_stock = Integer.parseInt(request.getParameter("flag_stock"));
            
             
        
           
           for(int y=0; y<Integer.parseInt(request.getParameter("NRO_FILAS"));y++){
               System.out.print("validando stock");
                 System.out.print(String.valueOf(flag_stock));
             
               int productocod = Integer.parseInt(request.getParameter("ProductoCod"+String.valueOf(y)));
               BigDecimal cantidad  = new BigDecimal(request.getParameter("Cantidad"+String.valueOf(y)));
               
               ProductoModel objProductoModel = new ProductoModel(empresaid);
               Producto objProducto = new Producto();      
               objProducto =  objProductoModel.searchProducto(productocod);
               System.out.print(objProducto.getProductostock());
               if(flag_stock==1 && objProducto.getProductostock()==1 ){
                   StockModel objStockModel =  new StockModel();
                       if(cantidad.compareTo(objStockModel.getSaldo(idBodega, objProducto.getProductoid()))>=0){
                           System.out.print("ERROR STOCK");
                           response.sendRedirect("messageview/errorstock.html");
                           return; 
                       }    
               }
               
            }
              
            boolean flag_dte = true;
          
           
           
           
       /* COMIENZO A RECOGER TOOS LOS DATOS DEL FORMULARIO  */
       
       /* VERIFICO LOS DATOS DEL CLIENTE */
            CliProvModel objCliProvModel = new CliProvModel(empresaid);      
            CliProv objCliProv = objCliProvModel.searchCliProv(Integer.parseInt(request.getParameter("CliProvCod")));
            
            /* PROCESO LOS DATOS DEL FORMULARIO */
            Movimiento objMovimiento = new Movimiento();
            objMovimiento.setObjcliprov(objCliProv);
            int exento = Integer.parseInt(request.getParameter("Exento"));
            int iva = Integer.parseInt(request.getParameter("Iva"));
            int totalneto = Integer.parseInt(request.getParameter("TotalNeto"));
            int totalbruto = Integer.parseInt(request.getParameter("TotalBruto"));
            int tipodocumento =  Integer.parseInt(request.getParameter("TipoDocumento"));
            int fpago = Integer.parseInt(request.getParameter("FPago"));
            int tpoventa =  Integer.parseInt(request.getParameter("TpoVenta"));
          
            
            
            objMovimiento.setFpago(fpago);
            objMovimiento.setTpoventa(tpoventa);
            
            objMovimiento.setFlag_stock(flag_stock);
            objMovimiento.setTipodoc(tipodocumento);
            DocumentoModel objDocumento = new DocumentoModel();
            String fechadoc = request.getParameter("FechaDoc");
            String codsii = String.valueOf(objDocumento.getSiiCod(tipodocumento));
            String campo="";
           
         
            String fchref =(String)request.getParameter("FchRef");
            
            objMovimiento.setFchref(fchref);
             
            
            int  tipodespachoid = Integer.parseInt(request.getParameter("TipoDespacho"));
            int   tipotrasladoid = Integer.parseInt(request.getParameter("TipoTraslado"));
            
            
             /* Despacho objDespacho = new Despacho(); */
           DespachoModel objDespachoModel = new DespachoModel();
           Despacho objDespacho = objDespachoModel.getData(tipodespachoid);
           objMovimiento.setTipodespacho(objDespacho);
           
           TrasladoModel objTrasladoModel = new TrasladoModel();
           Traslado objTraslado = objTrasladoModel.getData(tipotrasladoid);
           
           objMovimiento.setTipotraslado(objTraslado);
           objMovimiento.setReferenciaGlobal(request.getParameter("Observacion"));
           objMovimiento.setTpodocref(Integer.parseInt(request.getParameter("TpoDocRef")));
           objMovimiento.setFolioref(Integer.parseInt(request.getParameter("NumDocRef")));
           objMovimiento.setTporef(Integer.parseInt(request.getParameter("TpoRef")));
           objMovimiento.setBodegaid(idBodega);
           /*
            objDespacho.setDespachoid(tipodespachoid);
            Traslado objTraslado = new Traslado();
             */       
            switch (codsii) {
            
               case "33" -> campo = "FacVentaAfecta";
                       
               case "52" -> campo = "GuiaDespacho";
               case "56" -> campo = "NotaDebito";
               
               
               case "61" -> campo = "NotaCredito";
            
                 case "39" -> campo = "BolAfectaE";
                 
                 case "41" -> campo = "BolExentaE";     
                         
              
               
               case "34" -> campo = "FacVentaExenta";
                         
                         
               case "802" -> {
                   campo = "NotaPedido";
                   flag_dte = false;
                 }          
               
               case "46" -> {
                   response.sendRedirect("messageview/errordoc.html");          
                   return;
               }
               case "801" ->{ response.sendRedirect("messageview/errordoc.html");                    
                     return;   
               }
               }
           
          
          
          boolean validacionform = true;
          
          
            Funciones objfunciones = new Funciones();
          
            
            if(flag_dte ==true){
            
            /* ANTES DE INSERTAR EN LA BASE DE DATOS ME ASEGURO DE QUE LOS FOLIOS ELECTRONICOS ESTEN CARGADOS Y VALIDOS */    
            Funciones objFunciones = new Funciones();
            objFunciones.loadCAF(empresaid, tipodocumento, login);
                
                
            if(objfunciones.buscaFolios(login,empresaid, codsii)==false){
                validacionform = false;
                  response.sendRedirect("messageview/errorfolio.html");
                  return; 
            }
            }
        
            
             if(("34".equals(codsii)) & (totalneto > 0)){
                  validacionform = false;
                 response.sendRedirect("messageview/errormontoafecto.html");
                 return;
             }
         
              if(("33".equals(codsii)) & (totalneto==0)){
                  validacionform = false;
                 response.sendRedirect("messageview/errormontoafecto2.html");
                 return; 
            }
         
             
            ConfigAppDTE objConfig = new ConfigAppDTE();
          
            int numcorrelativo =  objDocumento.searchCorrelativo(empresaid, campo);
            objMovimiento.setFechamov(fechadoc);
            objMovimiento.setNumdoc(numcorrelativo);
            objMovimiento.setMontoexento(exento);
            objMovimiento.setMontoiva(iva);
            objMovimiento.setMontototal(totalbruto);
            objMovimiento.setMontoafecto(totalneto);
            MovimientoModel objMovimientoModel = new MovimientoModel();
            
           EmpresaModel objEmpresaModel = new EmpresaModel();
          
            Empresa objEmpresa = objEmpresaModel.getData(empresaid);
          
           FuncionesCAF objCAF = new FuncionesCAF();
          
           
           if(flag_dte==true){
            if (objCAF.validaCAF(login,objConfig.getPathcaf(), objEmpresa.getEmpresarut(),Integer.parseInt(codsii),numcorrelativo)==false){
                validacionform = false;
                response.sendRedirect("messageview/errorcaf.html");
             return; 
            }
           }
            
           if(validacionform==true){ 
            
            objMovimientoModel.addDocumento(empresaid, objCliProv.getCliprovid(), objMovimiento);
            
          int referenciaflag = (int) request.getSession().getAttribute("ReferenciaFlag");
          int auxidmovimiento = (int) request.getSession().getAttribute("MovimientoId");
            
          if(referenciaflag==1){
              objMovimientoModel.updateFlagReferencia(auxidmovimiento, referenciaflag);
          }
       
                 
            int idmovimiento = objMovimientoModel.searchId(objCliProv.getCliprovid(), tipodocumento, numcorrelativo);
            objDocumento.updateCorrelativo(empresaid, campo);
           
           // ArrayList<DetalleMovimiento> arraydetalle = new ArrayList<>();           
            int nrofilas = Integer.parseInt(request.getParameter("NRO_FILAS"));
             
              
            for(int i=0;i<nrofilas;i++){
               int productocod = Integer.parseInt(request.getParameter("ProductoCod"+String.valueOf(i)));
               ProductoModel objProductoModel = new ProductoModel(empresaid);
                             
               StockModel objStock = new StockModel();
               int bodegaid = Integer.parseInt(request.getParameter("Bodega")); 
               Producto objProducto =  objProductoModel.searchProducto(productocod);
               int idproducto = objProducto.getProductoid();
            
               DetalleMovimiento objDetalleMovimiento = new DetalleMovimiento();
               objDetalleMovimiento.setObjProducto(objProducto);
               objDetalleMovimiento.setCantidad(new BigDecimal(request.getParameter("Cantidad"+String.valueOf(i))));
               objDetalleMovimiento.setTotal(Integer.parseInt(request.getParameter("Total"+String.valueOf(i))));
             
               /* ejecuto la actualizacion del stock en este caso reduccion */
              if(!"61".equals(codsii) && flag_stock==1 ){
               BigDecimal SaldoAnterior = objStock.getSaldo(bodegaid, idproducto);
               objDetalleMovimiento.setSaldoanterior(SaldoAnterior);
               objStock.lowStock(bodegaid, idproducto,objDetalleMovimiento.getCantidad());
               BigDecimal SaldoActual = objStock.getSaldo(bodegaid, idproducto);
               objDetalleMovimiento.setSaldoactual(SaldoActual);
              }
                  
             
              if("61".equals(codsii) && flag_stock==1){
               BigDecimal SaldoAnterior = objStock.getSaldo(bodegaid, idproducto);
               objDetalleMovimiento.setSaldoanterior(SaldoAnterior);
               objStock.addStock(bodegaid, idproducto,objDetalleMovimiento.getCantidad());
               BigDecimal SaldoActual = objStock.getSaldo(bodegaid, idproducto);
               objDetalleMovimiento.setSaldoactual(SaldoActual);
          
              }
              
              if( flag_stock==0){
               BigDecimal SaldoAnterior = objStock.getSaldo(bodegaid, idproducto);
               objDetalleMovimiento.setSaldoanterior(SaldoAnterior);
               BigDecimal SaldoActual = objStock.getSaldo(bodegaid, idproducto);
               objDetalleMovimiento.setSaldoactual(SaldoActual);
          
              }
              
              
              
        
               objMovimientoModel.addDetalle(idmovimiento, objDetalleMovimiento);
            }
            /* inicio la secuencia de crear el xml */
          
            if(flag_dte==true){
            
            
            MovimientoController objMovimientoController;
           objMovimientoController = new MovimientoController();
           
           
          /* 
           String login = (String) request.getSession().getAttribute("login");
           */
             String trackid = objMovimientoController.sendDTE(objUsuario, empresaid, idmovimiento);
           
        if("0".equals(trackid)==true ){
               objMovimientoModel.deleteMovimiento(idmovimiento);
              response.sendRedirect("messageview/errorenvio.html");
            return; 
        }else{
           objMovimientoModel.updateTRACKID(idmovimiento, trackid);  
          
           
           String rutempresa = objEmpresa.getEmpresarut();
           
           
         
           
         /* SECCIONES DE IMPRESION EN PDF */   
         /* preparo la impresion del documento */
            BlobDTE objblob = new BlobDTE();
            objblob.getXMLDTE(idmovimiento);
         
          String[] arrayrutemisor = rutempresa.split("-");
         
          if("39".equals(codsii) || "41".equals(codsii)){
               PrintBOLETA objPrint = new PrintBOLETA();
            objPrint.printBOLETA(arrayrutemisor[0]+"F"+String.valueOf(numcorrelativo)+"T"+codsii);
              
          }else{
          
          PrintDTE objPrint = new PrintDTE();
          objPrint.printDTE(arrayrutemisor[0]+"F"+String.valueOf(numcorrelativo)+"T"+codsii);
        
          }
       
          //request.getSession().setAttribute("nombre_param", "valor_param");
           request.getSession().setAttribute("trackid",trackid);
           request.getSession().setAttribute("nombredocumento","ENVDTE"+arrayrutemisor[0]+"F"+String.valueOf(numcorrelativo)+"T"+codsii);
           request.getSession().setAttribute("tipovista","emision");
           response.sendRedirect("printdte");
          } 
        }else{  
                    //se indica que la respuesta sera HTML
                    
                    
         PrintNota objPrint = new PrintNota();
         objPrint.printPDF(idmovimiento, empresaid);              
         request.getSession().setAttribute("nombredocumento","NOTAPEDIDO");      
        
         response.sendRedirect("printnota");
            
          
         
            
            
            
            
            
            }    
        }
         } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
              response.setContentType("text/html;charset=UTF-8");
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR); // Establece el código de estado HTTP

            try (var writer = response.getWriter()) { // Uso de try-with-resources
                writer.println("<html>");
                writer.println("<head><title>ERROR</title>");
                writer.println("<style>body { font-family: Arial, sans-serif; background-color: #f8d7da; color: #721c24; }" +
                               "h1 { color: #c7254e; } " +
                               "p { font-size: 1.2em; }</style>"); // Estilos CSS básicos
                writer.println("</head>");
                writer.println("<body>");
                writer.println("<h1>ERROR</h1>");
                writer.println("<p>ERROR DE CONEXIÓN SII. Por favor, inténtelo más tarde.</p>");
                writer.println("<p><a href=\"javascript:history.back();\">Volver a la página anterior</a></p>"); // Enlace para volver atrás usando JavaScript
                writer.println("</body>");
                writer.println("</html>");
            }
      
             Logger.getLogger(addMovimientoServlet.class.getName()).log(Level.SEVERE, null, ex);
             
         } catch (IOException | NumberFormatException ex) {
              response.setContentType("text/html;charset=UTF-8");
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR); // Establece el código de estado HTTP

            try (var writer = response.getWriter()) { // Uso de try-with-resources
                writer.println("<html>");
                writer.println("<head><title>ERROR</title>");
                writer.println("<style>body { font-family: Arial, sans-serif; background-color: #f8d7da; color: #721c24; }" +
                               "h1 { color: #c7254e; } " +
                               "p { font-size: 1.2em; }</style>"); // Estilos CSS básicos
                writer.println("</head>");
                writer.println("<body>");
                writer.println("<h1>ERROR</h1>");
                writer.println("<p>ERROR DE CONEXIÓN SII. Por favor, inténtelo más tarde.</p>");
                writer.println("<p><a href=\"javascript:history.back();\">Volver a la página anterior</a></p>"); // Enlace para volver atrás usando JavaScript
                writer.println("</body>");
                writer.println("</html>");
            }
      
            Logger.getLogger(addMovimientoServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
             response.setContentType("text/html;charset=UTF-8");
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR); // Establece el código de estado HTTP

            try (var writer = response.getWriter()) { // Uso de try-with-resources
                writer.println("<html>");
                writer.println("<head><title>ERROR</title>");
                writer.println("<style>body { font-family: Arial, sans-serif; background-color: #f8d7da; color: #721c24; }" +
                               "h1 { color: #c7254e; } " +
                               "p { font-size: 1.2em; }</style>"); // Estilos CSS básicos
                writer.println("</head>");
                writer.println("<body>");
                writer.println("<h1>ERROR</h1>");
                writer.println("<p>ERROR DE CONEXIÓN SII. Por favor, inténtelo más tarde.</p>");
                writer.println("<p><a href=\"javascript:history.back();\">Volver a la página anterior</a></p>"); // Enlace para volver atrás usando JavaScript
                writer.println("</body>");
                writer.println("</html>");
            }
            Logger.getLogger(addMovimientoServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
                
            
     
     }
     

}
