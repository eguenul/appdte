/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appventas.movimientos;

import com.egga.appventas.bodega.Bodega;
import com.egga.appventas.bodega.BodegaModel;
import com.egga.appventas.cliprov.CliProv;
import com.egga.appventas.cliprov.CliProvModel;
import com.egga.appventas.documento.Documento;
import com.egga.appventas.documento.DocumentoModel;
import com.egga.appventas.fpago.FPago;
import com.egga.appventas.fpago.FPagoModel;
import com.egga.appventas.producto.Producto;
import com.egga.appventas.producto.ProductoModel;
import com.egga.appventas.referencia.Referencia;
import com.egga.appventas.referencia.ReferenciaModel;
import com.egga.appventas.tpoventa.TpoVenta;
import com.egga.appventas.tpoventa.TpoVentaModel;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class MovimientoService {
    
    private final int empresaid;
    
public MovimientoService(int empresaid){
    this.empresaid = empresaid;
    
}    
    

public ArrayList<Bodega> listBodegas() throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{  
         BodegaModel objBodegaModel = new BodegaModel(this.empresaid);
         return objBodegaModel.listallBodegas();
}



    
    public List<Despacho> listaDespacho() throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
    
       DespachoModel objDespacho = new DespachoModel();    
       List<Despacho> arraydespacho = objDespacho.listDespacho();
       return arraydespacho;
    }      
       
    
   public List<Traslado> listaTraslado() throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{ 
    
       TrasladoModel objTraslado = new TrasladoModel();    
       return objTraslado.listTraslado();
   }
       
    
   public List<Documento> listaDocumento () throws SQLException{
     DocumentoModel objDocumentoModel = new DocumentoModel();
     return objDocumentoModel.listDocuments();  
   }
      
   public ArrayList<CliProv> listaCliProv() throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException {
      
      CliProvModel objCliProvModel = new CliProvModel(this.empresaid);
      ArrayList<CliProv> arraylistcliprov = objCliProvModel.listaCliProv(0);
      
      return arraylistcliprov;
      
   }
   

   
   public ArrayList<Producto> listProducto() throws SQLException{
       
       
            ProductoModel objProductoModel = new ProductoModel(this.empresaid);
            ArrayList<Producto>  arrayproducto = objProductoModel.listProducto(0);
            return arrayproducto;
    }



public ArrayList<FPago> listFormaPago() throws SQLException{
   
    FPagoModel objFPagoModel =  new FPagoModel();
   return objFPagoModel.listFpago();
}



public ArrayList<TpoVenta> listTpoVenta() throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{

   TpoVentaModel objTpoVentaModel = new TpoVentaModel();
     return objTpoVentaModel.listTpoVenta();
}


public ArrayList<Referencia> listTpoReferencia() throws SQLException, ParserConfigurationException, SAXException, IOException{
    ReferenciaModel objReferenciaModel = new ReferenciaModel();
     ArrayList<Referencia> arrayreferencia = objReferenciaModel.listTpoReferencia();
     return arrayreferencia;
}



/*

      ReferenciaModel objReferenciaModel = new ReferenciaModel();
      ArrayList<Referencia> arrayreferencia = objReferenciaModel.listTpoReferencia();
      request.getSession().setAttribute("arrayreferencia",arrayreferencia);
     

*/
    


    
    
    
    
}
