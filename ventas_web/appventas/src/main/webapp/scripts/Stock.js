async function mostrarStock(ProductoCod, BodegaId) {
    try {
        const response = await $.ajax({
            type: 'POST',
            url: 'buscarStock',
            data: {
                ProductoCod: ProductoCod,
                BodegaId: BodegaId
            }
        });

        if (response && response.saldodisponible) {
            return response.saldodisponible;
        } else {
            throw new Error("No hay datos disponibles");
        }
    } catch (error) {
        console.error('Hubo un problema:', error);
        throw error;
    }
}

