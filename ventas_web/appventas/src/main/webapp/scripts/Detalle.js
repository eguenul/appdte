
function AgregaDetalle(ProductoCod, ProductoNom,PrecioCosto,PrecioVenta,ProductoIva,UnidadMedida){
  
	var tabla = document.getElementById("TablaDetalle").getElementsByTagName('tbody')[0];
        var BodegaId = document.getElementById('Bodega').value;
        var nro_filas =  parseInt(document.getElementById("NRO_FILAS").value);
       
// Create an empty <tr> element and add it to the 1st position of the table:
var FilaTabla =  document.createElement("tr");

	FilaTabla.id = "FilaTabla"+nro_filas;
	
// creo los elementos con estas etiquetas
    var Columna0 = document.createElement("td");
    var Columna1 = document.createElement("td");
    var Columna2 = document.createElement("td");
    var Columna3 = document.createElement("td");
    var Columna4 = document.createElement("td");
    var Columna5 = document.createElement("td");
    var Columna6 = document.createElement("td");
    var Columna7 = document.createElement("td");
    var Columna8 = document.createElement("td");
    
   
    var BtnEliminar = document.createElement("button");
    BtnEliminar.textContent = "Eliminar";
    BtnEliminar.className = "btn btn-danger"; // Clases de Bootstrap para estilizar el botón 
    BtnEliminar.type = "button";
    
    Columna8.appendChild(BtnEliminar);
    
   
    
    var Input0 = document.createElement("input");
    var Input1 = document.createElement("input");
    var Input2 = document.createElement("input");
    var Input3 = document.createElement("input");
    var Input4 = document.createElement("input");
    var Input5 = document.createElement("input");
    var Input6 = document.createElement("input");
    var Input7 = document.createElement("input");
    var Input8 = document.createElement("input");
	
	
	 // cambio los atributos de tamaño y solo lectura
         Input0.setAttribute('readonly', 'yes');
	 Input0.setAttribute('maxlength','8');
	 Input0.setAttribute('size','5');
	 Input1.setAttribute('size','15');
	 Input2.setAttribute('size','8');
	 Input3.setAttribute('size', '8');
	 Input4.setAttribute('size', '8');
	 Input5.setAttribute('size', '8');
	 Input6.setAttribute('size', '8');
	 Input8.setAttribute('size', '8');
	 
	 
	 Input1.setAttribute('readonly', 'yes');
	 Input2.setAttribute('readonly', 'yes');
	
         Input3.setAttribute('readonly', 'yes');
/*
         Input4.setAttribute('readonly', 'yes');
	 Input5.setAttribute('readonly', 'yes');
*/
	 Input8.setAttribute('readonly', 'yes');
         
         
         
          BtnEliminar.setAttribute('name', 'BtnEliminar'+nro_filas);
          BtnEliminar.setAttribute('id', 'BtnEliminar'+nro_filas);


	 // cambio los atributos de nombre 
	 Columna0.setAttribute('name', 'Columna0'+nro_filas);
	 Columna1.setAttribute('name', 'Columna1'+nro_filas);
	 Columna2.setAttribute('name', 'Columna2'+nro_filas);
	 Columna3.setAttribute('name', 'Columna3'+nro_filas);
	 Columna4.setAttribute('name', 'Columna4'+nro_filas);
	 Columna5.setAttribute('name', 'Columna5'+nro_filas);
	 Columna6.setAttribute('name', 'Columna6'+nro_filas);
         Columna7.setAttribute('name', 'Columna7'+nro_filas);
         Columna8.setAttribute('name', 'Columna8'+nro_filas);
         
         

	 Input0.setAttribute('name', 'ProductoCod'+nro_filas);
	 Input1.setAttribute('name', 'ProductoNom'+nro_filas);
	 Input2.setAttribute('name', 'UnidadMedida'+nro_filas);
	 Input3.setAttribute('name', 'ProductoPre'+nro_filas);
	 Input4.setAttribute('name', 'Porcentaje'+nro_filas);
	 Input5.setAttribute('name', 'Cantidad'+nro_filas);
	 
         Input6.setAttribute('name', 'Total'+nro_filas);
	 Input7.setAttribute('name', 'AfectoExento'+nro_filas);

	 Input8.setAttribute('name', 'Disponible'+nro_filas);

	 
	 
	 // cambio los atributos de id
	 Columna0.setAttribute('id', 'Columna0'+nro_filas);
	 Columna1.setAttribute('id', 'Columna1'+nro_filas);
	 Columna2.setAttribute('id', 'Columna2'+nro_filas);
	 Columna3.setAttribute('id', 'Columna3'+nro_filas);
	 Columna4.setAttribute('id', 'Columna4'+nro_filas);
	 Columna5.setAttribute('id', 'Columna5'+nro_filas);
	 Columna6.setAttribute('id', 'Columna6'+nro_filas);
         Columna7.setAttribute('id', 'Columna7'+nro_filas);
         Columna8.setAttribute('id', 'Columna8'+nro_filas);

	 Input0.setAttribute('id', 'ProductoCod'+nro_filas);
	 Input1.setAttribute('id', 'ProductoNom'+nro_filas);
	 Input2.setAttribute('id', 'UnidadMedida'+nro_filas);
	 Input3.setAttribute('id', 'ProductoPre'+nro_filas);
	 Input4.setAttribute('id', 'Porcentaje'+nro_filas);
	 Input5.setAttribute('id', 'Cantidad'+nro_filas);
	 Input6.setAttribute('id', 'Total'+nro_filas);
	 Input7.setAttribute('id', 'AfectoExento'+nro_filas);
         Input8.setAttribute('id', 'Disponible'+nro_filas);
          
          /* FUNCION ASINCRONA SE DEBE ESPERAR QUE SE CARGUE EL SERVLET Y DESPUES SEGUIR */
         (async () => {
    try {
        const stockdisponible = await mostrarStock(ProductoCod, BodegaId);
        Input8.value = stockdisponible;  // Asigna el valor al textbox
    } catch (error) {
        console.error('Error al obtener el stock:', error);
    }
})(); 
          
          
          
      
    
         Input7.setAttribute('type', 'hidden');
    

	 Input0.value = ProductoCod;
	 Input1.value = ProductoNom;
	 Input2.value = UnidadMedida;
         Input3.value = PrecioVenta;	 
	 Input4.value = 0;
	 Input5.value = 1;
	 Input6.value = PrecioVenta * 1;
	 Input7.value = ProductoIva;
	
	
        
         /* ASIGNO LOS CADA TEXT A SU RESPECTIVA COLUMNA */
	Columna0.appendChild(Input0);
	Columna1.appendChild(Input1);
	Columna2.appendChild(Input2);
	Columna3.appendChild(Input3);
	Columna4.appendChild(Input4);
	
	Columna5.appendChild(Input5);
	Columna7.appendChild(Input6);
	Columna6.appendChild(Input7);
        Columna6.append(Input8);
// ASIGNO LAS COLUMNAS A LA FILA
         FilaTabla.appendChild(Columna0);
         FilaTabla.appendChild(Columna1);
         FilaTabla.appendChild(Columna2);
         FilaTabla.appendChild(Columna3);
         FilaTabla.appendChild(Columna4);
         FilaTabla.appendChild(Columna5);
	 FilaTabla.appendChild(Columna6);
         FilaTabla.appendChild(Columna7);
         FilaTabla.appendChild(Columna8);
	
        
        
    // Crear un closure para mantener el valor correcto de nro_filas
    (function(nro_filas_closure) {
        
        
        BtnEliminar.onclick = function() {
         
            EliminaFila("FilaTabla" + nro_filas_closure);
        };
        
        Input4.onkeypress = function(){
            return filterFloat(event,this);
        };
        
        Input4.onkeyup = function(){
            if(Input5.value>0){ CalculaDescuento(nro_filas_closure);}
        };
        
        
        Input5.onkeypress = function(){
            return filterFloat(event,this);
        };
        
        Input5.onkeyup = function(){
           if(Input5.value>0){ CalculaDescuento(nro_filas_closure);}
        
         if(Input7.value===1){ CalculaDescuento(nro_filas_closure);}
          else{  
             Input6.value = Math.floor((eval(this.value)* eval(Input3.value)));
         } 
             if(this.value.length<1){ Input6.value=0;} 
        
        };
        
        /*
         FilaTabla.onclick = function(){var idFila = FilaTabla.id; CambiaColor(idFila);};  
	*/
        
        
    })(nro_filas);  // Pasamos el valor actual de nro_filas al cierre

      


                   
       

      $('#divproducto').modal('hide');

        tabla.appendChild(FilaTabla);
	nro_filas++;
	document.getElementById("NRO_FILAS").value = nro_filas;
     console.log('Después de agregar la fila, nro_filas es:', nro_filas);
     
     
		CalculoTotal();
}

function CambiaColor(parmId){
// ESTA FUNCION CAMBIA LAS FILAS DE COLORES

	nro_filas = eval(document.getElementById('NRO_FILAS').value);
	var i = 0;
	for(i=0; i<=nro_filas-1; i++){
	     	if(document.getElementById(parmId).id === ('FilaTabla'+i)){
			     idFila = 'FilaTabla' + i;
                document.getElementById('FilaTabla'+i).style.backgroundColor="#66ff33";
			}else{	
                document.getElementById('FilaTabla'+i).style.backgroundColor="#FFFFFF"; 
			}	

                		}
	
        }
	

function EliminaFila(auxIdFila){
	
        
        
        var auxNroFilas;
	var FilaTabla;
	
    var Columna0;
    var Columna1;
    var Columna2;
    var Columna3;
    var Columna4;
    var Columna5;
    var Columna6;
    var Columna7;
    var Columna8;
    
    
    var Input0; 
    var Input1;
    var Input2; 
    var Input3; 
    var Input4; 
    var Input5;
    var Input6;
    var Input8;
    var Input7;
    var BtnEliminar;
	// esta funcion renombra los elementos que se mantienen antes de que se eliminen
	
	auxNroFilas = parseInt(document.getElementById('NRO_FILAS').value);
	var i = 0;
	var j = 0;
	

	for(i=0; i<=auxNroFilas-1; i++){
      
	// OBTENGO LA REFERENCIA DE LOS ELEMENTOS
	 Columna0 = document.getElementById('Columna0'+i);
	 Columna1 = document.getElementById('Columna1'+i);
	 Columna2 = document.getElementById('Columna2'+i);
	 Columna3 = document.getElementById('Columna3'+i);
	 Columna4 = document.getElementById('Columna4'+i);
	 Columna5 = document.getElementById('Columna5'+i);
	 Columna6 = document.getElementById('Columna6'+i);
         Columna7 = document.getElementById('Columna7'+i);
         Columna8 = document.getElementById('Columna8'+i);
	 Input0 = document.getElementById('ProductoCod'+i);
	 Input1 = document.getElementById('ProductoNom'+i);
	 Input2 = document.getElementById('UnidadMedida'+i);
	 Input3 = document.getElementById('ProductoPre'+i);
	 Input4 = document.getElementById('Porcentaje'+i);
	 Input5 = document.getElementById('Cantidad'+i);
	 Input6 = document.getElementById('Total'+i);
	 Input7 = document.getElementById('AfectoExento'+i);
         Input8 = document.getElementById('Disponible'+i);
         BtnEliminar = document.getElementById('BtnEliminar'+i);
         
         
         
         FilaTabla = document.getElementById('FilaTabla'+i);     
	 FilaTabla.setAttribute('id','FilaTabla'+i);
	 FilaTabla.setAttribute('name','FilaTabla'+i);
	                    
	 Input8.setAttribute('id', 'Disponible'+i);	
         
      
         
         
         
         
	 var row = document.getElementById(FilaTabla.id);		 
	if(FilaTabla.id !== auxIdFila){
							 
	// cambio los atributos de nombre 
	 Columna0.setAttribute('name', 'Columna0'+j);
	 Columna1.setAttribute('name', 'Columna1'+j);
	 Columna2.setAttribute('name', 'Columna2'+j);
	 Columna3.setAttribute('name', 'Columna3'+j);
	 Columna4.setAttribute('name', 'Columna4'+j);
	 Columna5.setAttribute('name', 'Columna5'+j);
	 Columna6.setAttribute('name', 'Columna6'+j);
	 Columna7.setAttribute('name', 'Columna7'+j);
	 Columna8.setAttribute('name', 'Columna8'+j);
	
                
                
                
                
                
	 Input0.setAttribute('name', 'ProductoCod'+j);
	 Input1.setAttribute('name', 'ProductoNom'+j);
	 Input2.setAttribute('name', 'UnidadMedida'+j);
	 Input3.setAttribute('name', 'ProductoPre'+j);
	 Input3.setAttribute('name', 'Porcentaje'+j);
	 Input5.setAttribute('name', 'ProductoCan'+j);
	 Input6.setAttribute('name', 'Total'+j);
	 Input7.setAttribute('name', 'AfectoExento'+j);
	 Input8.setAttribute('name', 'Disponible'+j);
									
	 // cambio los atributos de id
	 Columna0.setAttribute('id', 'Columna0'+j);
	 Columna1.setAttribute('id', 'Columna1'+j);
	 Columna2.setAttribute('id', 'Columna2'+j);
	 Columna3.setAttribute('id', 'Columna3'+j);
	 Columna4.setAttribute('id', 'Columna4'+j);
	 Columna5.setAttribute('id', 'Columna5'+j);
	 Columna6.setAttribute('id', 'Columna6'+j);
         Columna7.setAttribute('id', 'Columna7'+j);
	 Columna8.setAttribute('id', 'Columna8'+j);
         
         
									 
	 Input0.setAttribute('id', 'ProductoCod'+j);
	 Input1.setAttribute('id', 'ProductoNom'+j);
	 Input2.setAttribute('id', 'UnidadMedida'+j);
	 Input3.setAttribute('id', 'ProductoPre'+j);
	 Input4.setAttribute('id', 'Porcentaje'+j);
	 Input5.setAttribute('id', 'Cantidad'+j);
	 Input6.setAttribute('id', 'Total'+j);
	 Input7.setAttribute('id', 'AfectoExento'+j);
	 Input8.setAttribute('id', 'Disponible'+j);
									
	 FilaTabla.setAttribute('id','FilaTabla'+j);
	 FilaTabla.setAttribute('name','FilaTabla'+j);
         
        (function(j_closure) {
        BtnEliminar.id = "BtnEliminar" + j;
        BtnEliminar.name = "BtnEliminar" + j;
        BtnEliminar.onclick = function() {
                EliminaFila("FilaTabla" + j_closure);
        };
        
        
        
        Input4.onkeypress = function(){
            return filterFloat(event,this);
        };
        
        Input4.onkeyup = function(){
            if(Input5.value>0){ CalculaDescuento(j);}
        };
        
        
        Input5.onkeypress = function(){
            return filterFloat(event,this);
        };
        
        Input5.onkeyup = function(){
           if(Input5.value>0){ CalculaDescuento(j);}
        
         if(Input7.value===1){ CalculaDescuento(j);}
          else{  
             Input6.value = Math.floor((eval(this.value)* eval(Input3.value)));
         } 
             if(this.value.length<1){ Input6.value=0;} 
        
        };
         })(j);
        
         
	 j++;
							 
	}else{
		    row.parentNode.removeChild(row);
		    document.getElementById("NRO_FILAS").value = auxNroFilas - 1;
			
            }
        }	
	
		CalculoTotal();
	
        }



function ValidaDetalle(){
	
	if(document.getElementById('CliProvCod').value.length<1){
            
            alert('Debe Validar Cliente/Proveedor');
            document.getElementById('CliProvCod').focus();
	    return false;
	}
	
	
	if(document.getElementById('ProductoCod').value.length<1){
           
		alert('Debe Validar Producto');
                 document.getElementById('ProductoCod').focus();
	    return false;
	}
	
	if(document.getElementById('Cantidad').value.length<1){
		alert('Cantidad debe ser mayor que cero');
                document.getElementById('Cantidad').focus();
	    return false;
	}
	
	
	if(document.getElementById('Porcentaje').value.length<1){
		alert('Si no hay descuento ingrese 0');
                document.getElementById('Porcentaje').focus();
	    return false;
	}
	
	
		return true;	
	
}


