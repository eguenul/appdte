<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.egga.appventas.empresa.Empresa"%>
<% List<Empresa> empresas = (ArrayList<Empresa>) request.getSession().getAttribute("arraylistempresa"); %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Selección Empresa</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/estilo.css">
    <style>
        body {
            background-color: #f5f5f5;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }
        .container {
            background: #fff;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0,0,0,0.15);
            padding: 2rem;
            max-width: 600px;
            width: 100%;
        }
        .container h1 {
            margin-bottom: 1.5rem;
            font-size: 24px;
            text-align: center;
        }
        .form-group {
            margin-bottom: 1rem;
        }
        .form-group label {
            font-weight: bold;
        }
        .btn-group {
            margin-top: 1rem;
            display: flex;
            justify-content: space-between;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Selección Empresa</h1>
        <form method="POST" action="empresa2">
            <div class="form-group">
                <label for="EmpresaId">Razón Social</label>
                <select id="EmpresaId" name="EmpresaId" class="form-control">
                    <%
                        for (Empresa empresa : empresas) {
                    %>
                    <option value="<% out.print(empresa.getEmpresaid()); %>"><% out.print(empresa.getEmpresaraz()); %></option>
                    <% } %>
                </select>
            </div>
            <div class="btn-group">
                <button type="submit" name="btnLimpiar" class="btn btn-primary">
                    <span class="glyphicon glyphicon-ok"></span> Seleccionar
                </button>
                <button type="button" onclick="window.location='empresa';" name="btnCrea" class="btn btn-default">
                    <span class="glyphicon glyphicon-file"></span> Nuevo
                </button>
            </div>
        </form>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
