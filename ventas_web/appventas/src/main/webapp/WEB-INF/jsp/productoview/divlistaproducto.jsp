<div id="divproducto" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Listado de Productos</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
          <table id="tabla-listado" class="table table-bordered">
            <thead>
              <tr>
                <th>C�digo</th>
                <th>Nombre</th>
                <th>SKU</th> <!-- Nueva columna para SKU -->
                <th>Acci�n</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <input id="ProductoCod2" name="ProductoCod2" class="form-control" 
                         onkeyup="if(this.value.length > 0) { cargarAjax('producto', 'ACC=BUSQUEDACOD&ProductoCod=' + this.value, 'listaproducto2'); }" 
                         onkeypress="return isNumberKey(event);">
                </td>
                <td>
                  <input id="ProductoNom2" name="ProductoNom2" class="form-control" 
                         onkeyup="if(this.value.length > 0) { cargarAjax('producto', 'ACC=BUSQUEDANOM&ProductoNom=' + this.value, 'listaproducto2'); }">
                </td>
                <td>
                  <input id="ProductoSKU2" name="ProductoSKU2" class="form-control"> <!-- Nuevo campo SKU -->
                </td>
                <td>
                  <button type="button" name="btnRefresh" id="btnRefresh" class="btn btn-primary btn-sm"
                          onclick="ProductoCod2.value=''; ProductoNom2.value=''; ProductoSKU2.value=''; cargarAjax('producto', 'ACC=REFRESH', 'listaproducto2');">
                    <span class="glyphicon glyphicon-refresh"></span> Refrescar
                  </button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        <div id="listaproducto">
          <br>
          <div id="listaproducto2">
            <%@ include file="../productoview/divlistaproducto2.jsp" %>
          </div>
        </div>
        <input type="hidden" id="pagina" name="pagina" value="">
      </div>
    </div>
  </div>
</div>
