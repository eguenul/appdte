<%@ page import="java.util.List" %>
<%@ page import="com.egga.appventas.producto.Producto" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<div class="table-responsive">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Código</th>
        <th>Nombre</th>
        <th>Precio Venta</th>
        <th>Acción</th>
      </tr>
    </thead>
    <tbody>
      <% 
      List<Producto> productos = (ArrayList<Producto>) request.getSession().getAttribute("arrayproducto");
      String modulo = (String) request.getSession().getAttribute("modulo");
      
      for (Producto producto : productos) {
      %>
      <tr>
        <td><%= producto.getProductocod() %></td>
        <td><%= producto.getProductonom() %></td>
        <td><%= producto.getPrecioventa() %></td>
        <td>
          <% if ("movimiento".equals(modulo) || "dtecompra".equals(modulo)) { %>
            <button class="btn btn-primary btn-sm" type="button"
                    onclick="AgregaDetalle('<%= producto.getProductocod() %>', '<%= producto.getProductonom() %>', <%= producto.getPreciocosto() %>, <%= producto.getPrecioventa() %>, <%= producto.getProductoiva() %>, '<%= producto.getUndmedidanom() %>');">
              <i class="fas fa-check"></i> Seleccionar
            </button>
          <% } else if ("movimientoprod".equals(modulo)) { %>
            <button class="btn btn-primary btn-sm" type="button"
                    onclick="document.getElementById('ProductoCod').value='<%= producto.getProductocod() %>'; document.getElementById('ProductoNom').value='<%= producto.getProductonom() %>'; cerrarModal();">
              <i class="fas fa-check"></i> Seleccionar
            </button>
          <% } else if ("producto".equals(modulo) || "merma".equals(modulo)) { %>
            <button class="btn btn-primary btn-sm" type="button"
                    onclick="document.formproducto.ProductoCod.value='<%= producto.getProductocod() %>'; document.formproducto.ACC.value='BUSCAR'; document.formproducto.submit();">
              <i class="fas fa-check"></i> Seleccionar
            </button>
          <% } else if ("addStock".equals(modulo)) { %>
            <button class="btn btn-primary btn-sm" type="button"
                    onclick="asignadetalle('<%= producto.getProductocod() %>', '<%= producto.getProductonom() %>', <%= producto.getPreciocosto() %>, <%= producto.getPrecioventa() %>, <%= producto.getProductoiva() %>, '<%= producto.getUndmedidanom() %>');">
              <i class="fas fa-check"></i> Seleccionar
            </button>
          <% } %>
        </td>
      </tr>
      <% } %> 
    </tbody>
  </table>
</div>
