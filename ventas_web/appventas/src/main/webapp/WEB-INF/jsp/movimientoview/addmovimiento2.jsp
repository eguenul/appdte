<%@page import="com.egga.appventas.referencia.Referencia"%>
<%@page import="com.egga.appventas.bodega.Bodega"%>
<%@page import="com.egga.appventas.tpoventa.TpoVenta"%>
<%@page import="com.egga.appventas.fpago.FPago"%>
<%@page import="com.egga.appventas.movimientos.Despacho"%>
<%@page import="com.egga.appventas.movimientos.DetalleMovimiento"%>
<%@page import="com.egga.appventas.movimientos.Movimiento"%>
<%@page import="com.egga.appventas.documento.Documento"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.egga.appventas.movimientos.Traslado"%>
<%@page import="com.egga.appventas.cliprov.CliProv"%>
<%@page import="java.util.*" %>
<%@page import="java.text.SimpleDateFormat"%>

<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Emisi&oacute;n DTE Venta</title>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"  href="css/estilo.css">
<script src="scripts/ajax.js"></script>
<script type="text/javascript" src="scripts/Numeros.js"></script>
<script src="scripts/cargastock.js"></script>
<script src="scripts/CalculoTotal.js"></script>
<script src="scripts/Detalle.js"></script>
<script src="scripts/Stock.js"></script>
<script src="scripts/Descuento.js"></script>
<script src="scripts/Movimiento.js"></script>
</head>
<body>  
<div class="container mt-4">
        <h1 class="text-center">Emisi&oacute;n DTE Venta</h1>
<div class="card mb-4">
        <div class="card-body">
            <h5 class="card-title">Empresa Seleccionada</h5>
            <p class="card-text">
                <strong>Razón Social:</strong>empresaa<br>
                <strong>RUT:</strong>13968481-8
            </p>
        </div>
    </div>
        <form method="POST" name="formmovimiento" action="movimiento" id="formmovimiento">
         
            <h4>Datos del Cliente/Proveedor</h4>
            <div class="form-group">
                <label for="CliProvCod">C&oacute;digo Cliente/Proveedor</label>
                <input value="<% out.print(request.getSession().getAttribute("CliProvCod")); %>"  type="text" name="CliProvCod" id="CliProvCod" class="form-control" value="C12345" readonly>
                <button type="button" onclick="window.location='index';" class="btn btn-secondary mt-2" id="btnLimpiarCliProv">Home</button>
           
            </div>
            <div class="form-group">
                <label for="CliProvRut">RUT Cliente/Proveedor</label>
                <input type="text" name="CliProvRut" id="CliProvRut" class="form-control" value="<% out.print(request.getSession().getAttribute("CliProvRaz")); %>" readonly>
            </div>
            <div class="form-group">
                <label for="CliProvRaz">Raz&oacute;n Social</label>
                <input type="text" value="<% out.print(request.getSession().getAttribute("CliProvRaz")); %>" name="CliProvRaz" id="CliProvRaz" class="form-control" value="Empresa Ejemplo S.A." readonly>
            </div>
            <div class="form-group">
                <label for="CliProvGiro">Giro</label>
                <input type="text" value="<% out.print(request.getSession().getAttribute("CliProvGir")); %>" name="CliProvGiro" id="CliProvGiro" class="form-control" value="Servicios InformÃ¡ticos" readonly>
            </div>
            <div class="form-group">
                <label for="CliProvCom">Comuna</label>
                <input type="text" value="<% out.print(request.getSession().getAttribute("CliProvCom")); %>"  name="CliProvCom" id="CliProvCom" class="form-control" value="Santiago" readonly>
            </div>
            <div class="form-group">
                <label for="CliProvDir">Direcci&oacute;n</label>
                <input type="text" value="<% out.print(request.getSession().getAttribute("CliProvDir")); %>" name="CliProvDir" id="CliProvDir" class="form-control" value="Av. Libertador 1234" readonly>
            </div>

            
            <!-- Nuevos Selects -->
            <h4>Detalles de la Venta</h4>
            <div class="form-group">
                <label for="FechaDoc">Fecha</label>
                <input type="date" " name="FechaDoc" id="FechaDoc" class="form-control">
            </div>

            
            <div class="form-group">
                <label for="TipoDocumento">Tipo Documento</label>
                <!--- -->
            </div>
            <div class="form-group">
                 <label for="BodegaId">Bodega</label>
                 <select class="form-control" id="Bodega" name="Bodega">
            <% 
             List<Bodega> arraylistbodega = (ArrayList<Bodega>)request.getSession(true).getAttribute("arraylistbodega");
                for (Bodega bodega: arraylistbodega){
           %>     
           <option value="<% out.print(bodega.getBodegaid());  %>"><% out.print(bodega.getBodeganom()); %></option>
            <% } %>
            </select>
            </div>
            <div class="form-group">
              <label for="flag_stock">Actualiza Stock</label>
                 <select class="form-control" id="flag_stock" name="flag_stock">
                     <option value="0">NO</option>
                     <option value="1">SI</option>                  
                 </select>   
            </div>
           
            <div class="form-group">
                <label for="TpoVenta">Tipo de Boleta</label>
                <select id="TpoVenta" name="TpoVenta" class="form-control">
                      <% 
             List<TpoVenta> listtpoventa = (ArrayList<TpoVenta>)request.getSession(true).getAttribute("arraytpoventa");
                for (TpoVenta tpoventa: listtpoventa){
           %>     
           <option value="<% out.print(tpoventa.getIdtpoventa()); %>"><% out.print(tpoventa.getDescripcion()); %></option>

                
                <% } %>

                </select>
            </div>
            
                
            
            <div class="form-group">
                <label for="FPago">Forma de Pago</label>
                <select id="FPago" name="FPago"  class="form-control" >
<%        List<FPago> listfpago = (ArrayList<FPago>)request.getSession(true).getAttribute("arrayfpago");
 %>               
                
            <%   for(FPago fpago : listfpago){ %>
             <option value="<% out.print(fpago.getIdfpago()); %>"><% out.print(fpago.getFpagodes()); %></option>
            <% } %>
            </select>

            </div>
            <div class="form-group">
                <label for="metodoPago">M&eacute;todo de Pago</label>
                <select id="metodoPago" name="metodoPago" class="form-control">
                    <option value="contado">Contado</option>
                    <option value="pagoParcial">Pago Parcial</option>
                    <option value="pagoDiferido">Pago Diferido</option>
                </select>
            </div>

            <h4>Datos del Producto</h4>
            
            <table id="TablaDetalle" class="table table-bordered table-striped mt-4">
                <thead>
                    <tr>
                        <th>C&oacute;digo</th>
                        <th>Producto</th>
                        
                        <!--
                        <th>Und Medida</th>
                        -->
                        <th>Precio</th>
                        <th>% Desc.</th>
                        <th>Cantidad</th>
                        <th>Total</th>
                       </tr>
                </thead>
                <tbody>
                    
                    
                    <!-- Input0.setAttribute('id', 'ProductoCod'+nro_filas);
                        Input1.setAttribute('id', 'ProductoNom'+nro_filas);
                        Input2.setAttribute('id', 'UnidadMedida'+nro_filas);
                        Input3.setAttribute('id', 'ProductoPre'+nro_filas);
                        Input4.setAttribute('id', 'Porcentaje'+nro_filas);
                        Input5.setAttribute('id', 'Cantidad'+nro_filas);
                        Input6.setAttribute('id', 'Total'+nro_filas);
                        Input7.setAttribute('id', 'AfectoExento'+nro_filas);
                        Input8.setAttribute('id', 'Disponible'+nro_filas);

                    -->
                    
                    
                    <% ArrayList<Object[]> arraydetalle = (ArrayList<Object[]>) request.getSession().getAttribute("arraydetalle"); %>
                    <% int j = 0;
                        for(Object[] i: arraydetalle){       
                      %>
                     <tr>
                         <td><input name="ProductoCod<% out.print(j); %>" size="8" readonly value="<% out.print(i[0]); %>"> </td>
                         <td><input name="ProductoNom<% out.print(j); %>" size="15" readonly value="<% out.print(i[1]); %>"></td>
                         <td><input name="ProductoPre<% out.print(j); %>"  size="8" readonly value="<% out.print(i[3]); %>"></td>
                         <td><input name="Porcentaje<% out.print(j); %>"  size="8" readonly value="<% out.print(i[4]); %>"></td>
                         <td><input name="Cantidad<% out.print(j); %>"  size="8" readonly value="<% out.print(i[2]); %>"></td>
                         <td><input name="Total<% out.print(j); %>"size="8" readonly value="<% out.print(i[6]); %>"></td>
                     </tr>
                        <% j++; %>
                     <%  }  %>
                </tbody>
            </table>

            <h4>Detalles de Venta</h4>
            <table id="tabla3" class="table table-bordered table-striped mt-4">
                <thead>
                    <tr>
                        <th colspan="2">Valores</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Neto</td>
                        <td><input name="TotalNeto" id="TotalNeto" readonly type="text" class="form-control" value="<% out.print(request.getSession().getAttribute("TotalNeto")); %>"></td>
                    </tr>
                    <tr>
                        <td>Exento</td>
                        <td><input name="Exento" id="Exento" readonly type="text" class="form-control" value="<% out.print(request.getSession().getAttribute("Exento")); %>"></td>
                    </tr>
                    <tr>
                        <td>IVA (19%)</td>
                        <td><input name="Iva" id="Iva" readonly type="text" class="form-control" value="<% out.print(request.getSession().getAttribute("Iva")); %>"></td>
                    </tr>
                    <tr>
                        <td>Total</td>
                        <td><input name="TotalBruto" id="TotalBruto" readonly type="text" class="form-control" value="<% out.print(request.getSession().getAttribute("Total")); %>"></td>
                    </tr>
                </tbody>
            </table>
            <input type="hidden" id="NRO_FILAS" name="NRO_FILAS" value="<% out.print(j); %>">
            <div class="form-group">
                <button type="button" onclick="GrabarMovimiento();" class="btn btn-primary">Emitir Factura</button>
            </div>
            <input type="hidden" name="ACC" id="ACC">
                
    <!-- Modal 1 -->
   <!-- Modal Despacho -->
<div id="ModalDespacho" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Datos de Despacho</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <label for="TipoDespacho">Tipo de Despacho</label>
            <select name="TipoDespacho" id="TipoDespacho" class="form-control">
              <% 
              List<Despacho> arraydespacho = (ArrayList<Despacho>) request.getSession(true).getAttribute("arraydespacho");
              for (Despacho i : arraydespacho) {
              %> 
                <option value="<% out.print(i.getDespachoid()); %>"><% out.print(i.getDespachodes()); %></option> 
              <% } %>
            </select>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Traslado -->


<!-- Modal -->
<div id="ModalTraslado" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tipo de Traslado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
          <div class="form-group">
            <label for="TipoTraslado">Tipo de Traslado</label>
            <select name="TipoTraslado" id="TipoTraslado" class="form-control">
              <% 
              List<Traslado> arraytraslado = (ArrayList<Traslado>) request.getSession(true).getAttribute("arraytraslado");
              for (Traslado i : arraytraslado) {
              %> 
                <option value="<% out.print(i.getTipotrasladoid()); %>"><% out.print(i.getTrasladodes()); %></option> 
              <% } %>
            </select>
          </div>
             </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
            <!-- dopcumentos de referencia -->
            <!-- Modal -->
<div id="ModalDoc" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Documento de Referencia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="TpoDocRef">Tipo de Documento</label>
          <input name="TpoDocRef" id="TpoDocRef" class="form-control">
            <!--- ---->
        </div>
        <div class="form-group">
          <label for="NumDocRef">Número de Documento</label>
          <input type="text" name="NumDocRef" id="NumDocRef" class="form-control" value="0">
        </div>
        <div class="form-group">
          <label for="FchRef">Fecha del Documento</label>
          <input type="date" name="FchRef" id="FchRef" class="form-control" value="">
        </div>
        <div class="form-group">
          <label for="TpoRef">Tipo de Referencia</label>
          <select name="TpoRef" id="TpoRef" class="form-control">
            <% 
              List<Referencia> arrayreferencia = (ArrayList<Referencia>) request.getSession(true).getAttribute("arrayreferencia");  
              for (Referencia auxreferencia : arrayreferencia) {  
            %>     
              <option value="<% out.print(auxreferencia.getReferenciaid()); %>" <% if (auxreferencia.getReferenciacod() == 0) { %> selected <% } %>>
                <% out.print(auxreferencia.getReferenciades()); %>
              </option>     
            <% } %>           
          </select>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

            <!-- Modal DE OBSERVACION -->
<div id="ModalObservacion" class="modal fade" role="dialog">
  <div class="modal-dialog" role="document">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Datos de Observación</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="Observacion">Observación</label>
          <textarea id="Observacion" name="Observacion" class="form-control" rows="10"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
</form>
</div>
<!-- Modal Cliente/Proveedor -->
<!-- Botones para abrir los nuevos modales -->
<div class="container mt-4">
        <h4>Informaci&oacute;n Adicional</h4>
        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#ModalDespacho">Datos Despacho</button>
        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#ModalTraslado">Datos de Traslado</button>
        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#ModalObservacion">Observaciones</button>
        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#ModalDoc">Documento de Referencia</button>
</div>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> 
</body>
</html>


