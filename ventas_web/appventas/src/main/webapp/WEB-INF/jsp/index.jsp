
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EGGA INFORMATICA - Inicio</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        /* Cambia el color de fondo de la barra de navegaci�n */
        .navbar {
            background-color: #007bff; /* Azul */
        }
        /* Cambia el color del texto de los enlaces principales a blanco */
        .navbar-nav .nav-link {
            color: #ffffff !important; /* Blanco */
        }
        /* Mantiene el color original de los men�s desplegables */
        .dropdown-item {
            color: #212529; /* Color oscuro por defecto */
        }
        .dropdown-item:hover {
            color: #ffffff; /* Cambia a blanco al pasar el mouse */
            background-color: #007bff; /* Azul */
        }
        /* Cambia el color de los textos adicionales a blanco */
        .navbar-text {
            color: #ffffff; /* Blanco */
        }
    </style>
</head>
<body>

<!-- Navbar Principal -->
<nav class="navbar navbar-expand-lg navbar-light">
    <a class="navbar-brand" href="#">EGGA INFORMATICA</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    MANTENCI�N
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="empresa">EMPRESAS</a>
                    <a class="dropdown-item" href="producto">PRODUCTOS</a>
                    <a class="dropdown-item" href="cliprov">CLIENTE/PROVEEDOR</a>
                    <a class="dropdown-item" href="addBodega">BODEGAS</a>
                    <a class="dropdown-item" href="adminUndMedida">UNIDADES DE MEDIDA</a>
                    <a class="dropdown-item" href="usuario">USUARIOS</a>
                    <a class="dropdown-item" href="adminCAF">ADMINISTRACION CAF</a>
                    <a class="dropdown-item" href="adminCert">CARGA CERTIFICADO DIGITAL</a>
                    <a class="dropdown-item" href="setFirmaPass">CLAVE FIRMA ELECTRONICA</a>
                    <a class="dropdown-item" href="correlativo">CORRELATIVOS</a>
                    <a class="dropdown-item" href="setpass">CAMBIO PASSWORD ADMINISTRADOR</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="puntoDeVentaDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    PUNTO DE VENTA
                </a>
                <div class="dropdown-menu" aria-labelledby="puntoDeVentaDropdown">
                    <a class="dropdown-item" href="boleta">EMITIR BOLETA DE VENTA</a>
                    <a class="dropdown-item" href="notaventa">EMITIR NOTA DE PEDIDO</a>
                    <a class="dropdown-item" href="factura">EMITIR FACTURA</a>
                    <a class="dropdown-item" href="guiadespacho">EMITIR GUIA DE DESPACHO</a>
                    <a class="dropdown-item" href="boletaexenta">EMITIR BOLETA EXENTA</a>
                    <a class="dropdown-item" href="notacredito">EMITIR NOTA DE CREDITO</a>
                    <a class="dropdown-item" href="notadebito">EMITIR NOTA DE DEBITO</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="procesosDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    PROCESOS
                </a>
                <div class="dropdown-menu" aria-labelledby="procesosDropdown">
                    <a class="dropdown-item" href="movimiento">EMISI�N DTE VENTA</a>
                    <a class="dropdown-item" href="dtecompra">EMISI�N DTE COMPRA</a>
                    <a class="dropdown-item" href="notapedido">EMISI�N DTE DESDE NOTA PEDIDO</a>
                    <a class="dropdown-item" href="adminStock">CARGA STOCK</a>
                    <a class="dropdown-item" href="Merma">REGISTRO DE MERMAS</a>
                    <a class="dropdown-item" href="selempresa">SELECCI�N EMPRESA</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="informesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    INFORMES
                </a>
                <div class="dropdown-menu" aria-labelledby="informesDropdown">
                    <a class="dropdown-item" href="libroventas">LIBRO DE VENTAS</a>
                    <a class="dropdown-item" href="historialdte">HISTORIAL DTE</a>
                    <a class="dropdown-item" href="informestock">STOCK EN BODEGA</a>
                    <a class="dropdown-item" href="movimientoprod">INFORME MOVIMIENTO POR PRODUCTO</a>
                    <a class="dropdown-item" href="vtaproducto">INFORME VENTAS POR PRODUCTO</a>
                    <a class="dropdown-item" href="vtasCliProv">INFORME VENTAS POR CLIENTE</a>
                </div>
            </li>
        </ul>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="#">egueneul</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="logout.jsp">SALIR</a>
            </li>
        </ul>
    </div>
</nav>

<!-- Card de Informaci�n de la Empresa -->
<div class="container mt-4">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Raz�n Social</h5>
            <p class="card-text">EGGA INFORMATICA E.I.R.L</p>
            <h5 class="card-title">RUT</h5>
            <p class="card-text">76040308-3</p>
        </div>
    </div>
</div>

<div class="container mt-4">
    <h1>Bienvenido a EGGA INFORMATICA</h1>
    <p>Selecciona una opci�n del men� para continuar.</p>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
