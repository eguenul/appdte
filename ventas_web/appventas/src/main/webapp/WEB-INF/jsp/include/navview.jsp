<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">APPDTE</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-cog"></span> MANTENCI�N <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="empresa">Empresas</a></li>
                        <li><a href="producto">Productos</a></li>
                        <li><a href="cliprov">Cliente/Proveedor</a></li>
                        <li><a href="addBodega">Bodegas</a></li>
                        <li><a href="adminUndMedida">Unidades de Medida</a></li>
                        <li><a href="usuario">Usuarios</a></li>
                        <li><a href="adminCAF">Administraci�n CAF</a></li>
                        <li><a href="adminCert">Carga Certificado Digital</a></li>
                        <li><a href="setFirmaPass">Clave Firma Electr�nica</a></li>
                        <li><a href="correlativo">Correlativos</a></li>
                        <li><a href="setpass">Cambio Password Administrador</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-tasks"></span> Punto de Venta <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="boletaservlet">Emisi�n Boleta de Venta</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-tasks"></span> Procesos <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="movimiento">Emisi�n DTE Venta</a></li>
                        <li><a href="dtecompra">Emisi�n DTE Compra</a></li>
                        <li><a href="notapedido">Emisi�n DTE desde Nota Pedido</a></li>
                        <li><a href="adminStock">Carga Stock</a></li>
                        <li><a href="Merma">Registro de Mermas</a></li>
                        <li><a href="selempresa">Selecci�n Empresa</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-print"></span> Informes <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="libroventas">Libro de Ventas</a></li>
                        <li><a href="busquedafecha">Historial DTE</a></li>
                        <li><a href="informestock">Stock en Bodega</a></li>
                        <li><a href="movimientoprod">Informe Movimiento por Producto</a></li>
                        <li><a href="vtaproducto">Informe Ventas por Producto</a></li>
                        <li><a href="vtasCliProv">Informe Ventas por Cliente</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <span class="navbar-text">
                        Raz�n Social: 
                    </span>
                </li>
                <li>
                    <span class="navbar-text">
                        RUT: <%= request.getSession().getAttribute("EmpresaRut") %>
                    </span>
                </li>
                <li><a href="#">
                    <span class="glyphicon glyphicon-user"></span>
                    <%= request.getSession().getAttribute("login") %>
                </a></li>
                <li><a href="logout.jsp">
                    <span class="glyphicon glyphicon-log-out"></span> Salir
                </a></li>
            </ul>
        </div>
    </div>
</nav>
