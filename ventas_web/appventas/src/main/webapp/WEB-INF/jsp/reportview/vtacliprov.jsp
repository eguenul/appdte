<% CliProv objcliprov = (CliProv)request.getSession().getAttribute("objcliprov");       %>
<!DOCTYPE html>
<html>
<head>
<title>TODO supply a title</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> 
<link rel="stylesheet" type="text/css" href="css/estilo.css" media="screen" />
<script src="scripts/ajax.js"></script>
<script src="scripts/Fecha.js"></script>
<script src="scripts/ajax.js"></script>
<script src="scripts/vtasCliProv.js"></script>
<script>
   function cerrarModal(){
    $("#divcliprov").modal('hide');
   }
</script>
    </head>
    <body>
         <%@include file="../include/navview.jsp" %>
      <div align="center">
        <h1>
        VENTAS POR CLIENTE/PROVEEDOR
        </h1>
<div class="container">
    <form class="navbar-form" name=" formmovimiento" id="formmovimiento" action="" method="post">
        <fieldset>
                <legend>CLIENTE/PROVEEDOR SELECCIONADO:</legend>
               	<label for="CliProvCod">C�digo:</label>
                <input value="<% if(objcliprov.getCliprovcod()==0){out.print(""); }else{   out.print(objcliprov.getCliprovcod());} %>"   name="CliProvCod" id="CliProvCod">
                <button type="button" name="btnListadoProductos" id="btnListadoProductos" data-toggle="modal" data-target="#divcliprov"    class="btn btn-primary btn-sm">
                <span class="glyphicon glyphicon-search"></span>Buscar 
                </button>
                <label for="CliProvRaz">Producto:</label>
		<input name="CliProvRaz" value="<% out.print(objcliprov.getCliprovraz()); %>" id="CliProvRaz">
               <label for="CliProvRut">RUT:</label>
		<input name="CliProvRut" value="<% out.print(objcliprov.getCliprovrut()); %>" id="CliProvRut">
               
         </fieldset>
        <br><br><br>
        <fieldset>          
		<legend>RANGO DE BUSQUEDA:</legend>
               	<label for="FechaDesde">Desde:</label>
                <input   type="date" name="FechaDesde" id="FechaDesde">
                <label for="FechaHasta">Hasta:</label>
		<input  type="date" name="FechaHasta" id="FechaHasta">
                Formato:
                <select id="TipoReporte" name="TipoReporte" class="style-select">
                    <option value="pdf">PDF</option>
                    <option value="xls">EXCEL</option>
                </select>
                    <button class="btn btn-primary btn-sm" type="Button" onclick="CargaReporte();">
                 <span class="glyphicon glyphicon-search"></span>
                    Buscar</button>
               <button class="btn btn-primary btn-sm" type="Button" onclick="window.location='vtasCliProv';">
                 <span class="glyphicon glyphicon-file"></span>
                    Nuevo</button>
                
                <button class="btn btn-primary btn-sm" type="Button" onclick="window.location='index.jsp';">
                 <span class="glyphicon glyphicon-home"></span>
                    Home</button>  
        </fieldset>  
        <input hidden name="ACC" id="ACC" value="BUSCAR">
</form>
                
<div id="reporte">
</div>
</div>
</div>
        
        
<%@include file="../cliprovview/divlistacliprov.jsp" %>
        
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    </body>
</html>


