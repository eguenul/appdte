<%@page import="java.util.List"%>
<%@page import="com.egga.appventas.cliprov.CliProv"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div class="table-responsive">
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>CODIGO</th>
                <th>RAZON SOCIAL</th>
                <th>RUT</th>
                <th>ACCION</th>
            </tr>
        </thead>
        <tbody>
            <% 
                List<CliProv> cliprovs = (ArrayList<CliProv>)request.getSession(true).getAttribute("arraylistcliprov");
                for(CliProv cliprov : cliprovs) { 
            %>
            <tr>
                <td><%= cliprov.getCliprovcod() %></td>
                <td><%= cliprov.getCliprovraz() %></td>
                <td><%= cliprov.getCliprovrut() %></td>

                <!-- Acción según el módulo -->
                <% String modulo = (String) request.getSession().getAttribute("modulo"); %>
                <td>
                    <% if ("movimiento".equals(modulo)) { %>
                        <button class="btn btn-primary btn-sm" type="button"
                            onclick="ACC.value='BUSCAR'; CliProvCod.value='<%= cliprov.getCliprovcod() %>'; document.getElementById('formmovimiento').submit();">
                            <i class="fas fa-check"></i> Seleccionar
                        </button>
                    <% } else if ("dtecompra".equals(modulo)) { %>
                        <button class="btn btn-primary btn-sm" type="button"
                            onclick="ACC.value='BUSCAR'; CliProvCod.value='<%= cliprov.getCliprovcod() %>'; document.getElementById('formmovimiento').action='dtecompra'; document.getElementById('formmovimiento').submit();">
                            <i class="fas fa-check"></i> Seleccionar
                        </button>
                    <% } else if ("addStock".equals(modulo)) { %>
                        <button class="btn btn-primary btn-sm" type="button"
                            onclick="ACC.value='BUSCAR'; CliProvCod.value='<%= cliprov.getCliprovcod() %>'; document.getElementById('formmovimiento').action='adminStock'; document.getElementById('formmovimiento').submit();">
                            <i class="fas fa-check"></i> Seleccionar
                        </button>
                    <% } else if ("merma".equals(modulo)) { %>
                        <button class="btn btn-primary btn-sm" type="button"
                            onclick="ACC.value='BUSCAR'; CliProvCod.value='<%= cliprov.getCliprovcod() %>'; document.getElementById('formmovimiento').action='Merma'; document.getElementById('formmovimiento').submit();">
                            <i class="fas fa-check"></i> Seleccionar
                        </button>
                    <% } else if ("cesion".equals(modulo)) { %>
                        <button class="btn btn-primary btn-sm" type="button"
                            onclick="ACC.value='BUSCAR'; CliProvCod.value='<%= cliprov.getCliprovcod() %>'; document.getElementById('formcesion').submit();">
                            <i class="fas fa-check"></i> Seleccionar
                        </button>
                    <% } else if ("cliprov".equals(modulo)) { %>
                        <button class="btn btn-primary btn-sm" type="button"
                            onclick="ACC.value='BUSCAR'; CliProvCod.value='<%= cliprov.getCliprovcod() %>'; document.getElementById('formcliprov').submit();">
                            <i class="fas fa-check"></i> Seleccionar
                        </button>
                    <% } else if ("pos".equals(modulo)) { %>
                        <button class="btn btn-primary btn-sm" type="button"
                            onclick="auxCliProvCod.value='<%= cliprov.getCliprovcod() %>'; auxCliProvEma.value='<%= cliprov.getCliprovema() %>'; auxCliProvNom.value='<%= cliprov.getCliprovraz() %>'; auxCliProvDir.value='<%= cliprov.getCliprovdir() %>'; auxCliProvFon.value='<%= cliprov.getCliprovfon() %>'; $('#divcliprov').modal('hide')">
                            <i class="fas fa-check"></i> Seleccionar
                        </button>
                    <% } %>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
</div>
