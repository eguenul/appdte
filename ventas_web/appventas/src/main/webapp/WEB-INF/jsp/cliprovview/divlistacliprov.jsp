<!-- Modal Cliente/Proveedor -->
<div id="divcliprov" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Listado Cliente/Proveedor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>C�digo</th>
                                <th>Raz�n Social</th>
                                <th>RUT</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <input id="CliProvCod2" size="8" name="CliProvCod2" class="form-control"
                                        onkeypress="return isNumberKey(event);"
                                        onkeyup="if(this.value.length > 0) { cargarAjax('cliprov', 'ACC=BUSQUEDACOD&CliProvCod=' + this.value, 'contenido'); }">
                                </td>
                                <td>
                                    <input id="CliProvRaz2" size="15" name="CliProvRaz2" class="form-control"
                                        onkeyup="if(this.value.length > 0) { cargarAjax('cliprov', 'ACC=BUSQUEDARAZ&CliProvRaz=' + this.value, 'contenido'); }">
                                </td>
                                <td>
                                    <input size="8" id="CliProvRut2" name="CliProvRut2" class="form-control"
                                        onkeyup="if(this.value.length > 0) { cargarAjax('cliprov', 'ACC=BUSQUEDARUT&CliProvRut=' + this.value, 'contenido'); }">
                                </td>
                                <td>
                                    <button class="btn btn-primary btn-sm"
    onclick="CliProvCod2.value=''; CliProvRaz2.value=''; CliProvRut2.value=''; cargarAjax('cliprov', 'ACC=REFRESH', 'contenido');"
    type="button" name="btnRefresh" id="btnRefresh">
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-clockwise" viewBox="0 0 16 16">
      <path d="M8 3.5a4.5 4.5 0 1 1-3.535 1.465h1.69A3.001 3.001 0 0 0 8 5a3 3 0 1 0 3 3h-1.6A4.5 4.5 0 0 1 8 3.5z"/>
      <path d="M15.5 8a7.5 7.5 0 1 1-9.496-6.981l.992.885A6.5 6.5 0 1 0 8 14a6.5 6.5 0 0 0 6.5-6.5h-1z"/>
    </svg>
</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div id="listacliprov">
                    <br>
                    <div id="contenido">
                        <%@include file="../cliprovview/divlistacliprov2.jsp" %>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
